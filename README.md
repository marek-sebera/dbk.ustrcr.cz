# Evidence Osob, Míst, Událostí a Archivních informací
## amen.ustrcr.cz

- Instalace podle https://book.cakephp.org/4/en/installation.html
- Důležité balíčky (Debian) nginx-full, php-intl/php-mbstring/php-xml/php-mysql/php-redis, redis, mariadb-server
- Provozní / Administrátorská dokumentace je v [docs/provozni-administratorska-dokumentace.md](../docs/provozni-administratorska-dokumentace.md)
- Originální Konfigurace serveru v [docs/etc](../docs/etc/)

## API

Veřejně dostupné API je na adrese https://dbk.ustrcr.cz/api

Na této stránce je také popis API vč. dostupných URL a template pro jejich volání. API poskytuje jen read-only přístup k datům v DB,
ve struktuře definované v ORM, nic víc

## Licence a užití
Projekt je realizován dle smlouvy 120200056, č.j. USTR 264-2/2020, vlastníkem zdrojového kódu a práv je Ústav pro studium totalitních režimů,
kontaktní osobou ohledně realizace nebo dotazy právního charakteru je vedení ústavu, zejména Mgr. Ondřej Matějka, příp. oddělení informatiky

Projekt je zveřejněn pro nekomerční užití a šíření, v licenci AGPLv3, v případě komerčního využití je potřeba cílového zákazníka
informovat o příslušné licenci a podmínek z ní plynoucích
