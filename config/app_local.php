<?php
return [
    'debug' => false,
    'Security' => [
        'salt' => '1ef00b93913c63142b88f58f50ec6fc4d275ca2ec58b3733ac6daf4d20de9d5d',
    ],
    'Datasources' => [
        'default' => [
            'host' => 'localhost',

            'username' => 'amen_app',
            'password' => 'f6091836c25dead6be1d6892dbb6090fb19c1c5d',
            'database' => 'amen',

            'timezone' => 'Europe/Prague'
        ],
    ],
    'EmailTransport' => [
        'default' => [
            'host' => 'localhost',
            'port' => 25,
            'username' => null,
            'password' => null,
            'client' => null,
        ],
    ],
];
