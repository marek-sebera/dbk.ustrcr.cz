<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

return function (RouteBuilder $routes): void {

    $routes->setRouteClass(DashedRoute::class);

    $routes->scope('/', function (RouteBuilder $routes) {
        // CSRF
        $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
            'httponly' => true,
            'secure' => true,
            'cookieName' => '__Host-ustrCSRF'
        ]));

        $routes->applyMiddleware('csrf');

        $routes->connect('/', ['controller' => 'Public', 'action' => 'index']);

        $routes->connect('/api', ['controller' => 'Api', 'action' => 'index']);
        $routes->connect('/api/projects', ['controller' => 'Api', 'action' => 'projects']);
        $routes->connect('/api/project/{project_id}', ['controller' => 'Api', 'action' => 'project'])->setPass(['project_id']);
        $routes->connect('/api/project/{project_id}/type/{type_id}', ['controller' => 'Api', 'action' => 'projectType'])->setPass(['project_id', 'type_id']);
        $routes->connect('/api/project/{project_id}/type/{type_id}/instances', ['controller' => 'Api', 'action' => 'projectTypeInstances'])->setPass(['project_id', 'type_id']);

        $routes->connect('/login', ['controller' => 'Users', 'action' => 'login'], ['_name' => 'login']);
        $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout'], ['_name' => 'logout']);
        $routes->connect('/register', ['controller' => 'Users', 'action' => 'register'], ['_name' => 'register']);
        $routes->connect('/password_reset', ['controller' => 'Users', 'action' => 'passwordReset'], ['_name' => 'password_recovery']);
        $routes->connect('/password_reset/do/{token}/{requestId}', ['controller' => 'Users', 'action' => 'passwordResetGo'], ['_name' => 'password_recovery_go']);

        $routes->connect('/admin/', ['controller' => 'Admin', 'action' => 'dashboard'], ['_name' => 'admin_dashboard']);
        $routes->connect('/admin/user/{id}/disable', ['controller' => 'Admin', 'action' => 'userSetEnabled', 'state' => 'disabled'], ['_name' => 'admin_disable_user'])->setPass(['id', 'state']);
        $routes->connect('/admin/user/{id}/enable', ['controller' => 'Admin', 'action' => 'userSetEnabled', 'state' => 'enabled'], ['_name' => 'admin_enable_user'])->setPass(['id', 'state']);
        $routes->connect('/admin/user/{id}/set-administrator/yes', ['controller' => 'Admin', 'action' => 'userAdminSet', 'state' => 'enabled'], ['_name' => 'admin_set_is_admin'])->setPass(['id', 'state']);
        $routes->connect('/admin/user/{id}/set-administrator/no', ['controller' => 'Admin', 'action' => 'userAdminSet', 'state' => 'disabled'], ['_name' => 'admin_set_is_not_admin'])->setPass(['id', 'state']);
        $routes->connect('/admin/user/{id}/set-designer/yes', ['controller' => 'Admin', 'action' => 'userDesignerSet', 'state' => 'enabled'], ['_name' => 'admin_set_is_designer'])->setPass(['id', 'state']);
        $routes->connect('/admin/user/{id}/set-designer/no', ['controller' => 'Admin', 'action' => 'userDesignerSet', 'state' => 'disabled'], ['_name' => 'admin_set_is_not_designer'])->setPass(['id', 'state']);
        $routes->connect('/admin/project/{id}/set-public', ['controller' => 'Admin', 'action' => 'projectPublicSet', 'state' => 'enabled'], ['_name' => 'admin_project_set_public'])->setPass(['id', 'state']);
        $routes->connect('/admin/project/{id}/set-private', ['controller' => 'Admin', 'action' => 'projectPublicSet', 'state' => 'disabled'], ['_name' => 'admin_project_set_private'])->setPass(['id', 'state']);

        $routes->connect('/me/edit', ['controller' => 'Users', 'action' => 'editSelf'], ['_name' => 'update_self_info']);
        $routes->connect('/me/password', ['controller' => 'Users', 'action' => 'changePassword'], ['_name' => 'change_password']);

        $routes->connect('/users/{id}', ['controller' => 'Users', 'action' => 'detail'], ['pass' => ['id' => 'cast_number'], 'id' => '\d+']);

        $routes->connect('/projects', ['controller' => 'Projects', 'action' => 'index'], ['_name' => 'projects']);
        $routes->connect('/projects/add', ['controller' => 'Projects', 'action' => 'addModify'], ['_name' => 'project_add']);
        $routes->connect('/projects/add_example', ['controller' => 'Projects', 'action' => 'addExample'], ['_name' => 'project_add_example']);
        $routes->connect('/projects/{id}', ['controller' => 'Projects', 'action' => 'detail'], ['_name' => 'project_detail', 'pass' => ['id'], 'id' => '\d+']);
        $routes->connect('/projects/{id}/edit', ['controller' => 'Projects', 'action' => 'addModify'], ['_name' => 'project_edit', 'pass' => ['id'], 'id' => '\d+']);
        $routes->connect('/projects/{id}/structure', ['controller' => 'Projects', 'action' => 'projectStructure'], ['_name' => 'project_structure', 'pass' => ['id'], 'id' => '\d+']);
        $routes->connect('/projects/{id}/delete', ['controller' => 'Projects', 'action' => 'delete'], ['_name' => 'project_delete', 'pass' => ['id'], 'id' => '\d+']);

        $routes->connect('/projects/{project_id}/types/{type_id}/instances/add', ['controller' => 'Instances', 'action' => 'addModify'], ['_name' => 'instance_add', 'pass' => ['project_id', 'type_id']]);
        $routes->connect('/projects/{project_id}/types/{type_id}/instances/{instance_id}/edit', ['controller' => 'Instances', 'action' => 'addModify'], ['_name' => 'instance_edit', 'pass' => ['project_id', 'type_id', 'instance_id']]);
        $routes->connect('/projects/{project_id}/types/{type_id}/instances/{instance_id}', ['controller' => 'Instances', 'action' => 'detail'], ['_name' => 'instance_detail', 'pass' => ['project_id', 'type_id', 'instance_id']]);
        $routes->connect('/projects/{project_id}/types/{type_id}/instances/{instance_id}/delete', ['controller' => 'Instances', 'action' => 'delete'], ['_name' => 'instance_delete', 'pass' => ['project_id', 'type_id', 'instance_id']]);

        $routes->connect('/projects/{project_id}/types/add', ['controller' => 'Types', 'action' => 'addModify'], ['_name' => 'types_add', 'pass' => ['project_id'], 'project_id' => '\d+']);
        $routes->connect('/projects/{project_id}/types/{type_id}/edit', ['controller' => 'Types', 'action' => 'addModify'], ['_name' => 'types_edit', 'pass' => ['project_id', 'type_id'], 'project_id' => '\d+', 'type_id' => '\d+']);
        $routes->connect('/projects/{project_id}/types/{type_id}/delete', ['controller' => 'Types', 'action' => 'delete'], ['_name' => 'types_delete', 'pass' => ['project_id', 'type_id'], 'project_id' => '\d+', 'type_id' => '\d+']);
        $routes->connect('/projects/{project_id}/types/{type_id}/everything', ['controller' => 'Types', 'action' => 'allInstances'], ['_name' => 'types_everything', 'pass' => ['project_id', 'type_id'], 'project_id' => '\d+', 'type_id' => '\d+']);

        $routes->connect('/projects/{project_id}/types/{type_id}/groups/add', ['controller' => 'TypePropertiesGroups', 'action' => 'addModify'], ['_name' => 'type_properties_groups_add', 'pass' => ['project_id', 'type_id']]);
        $routes->connect('/projects/{project_id}/types/{type_id}/groups/{group_id}/edit', ['controller' => 'TypePropertiesGroups', 'action' => 'addModify'], ['_name' => 'type_properties_groups_edit', 'pass' => ['project_id', 'type_id', 'group_id']]);
        $routes->connect('/projects/{project_id}/types/{type_id}/groups/{group_id}/delete', ['controller' => 'TypePropertiesGroups', 'action' => 'delete'], ['_name' => 'type_properties_groups_delete', 'pass' => ['project_id', 'type_id', 'group_id']]);

        $routes->connect('/projects/{project_id}/types/{type_id}/metadata_types/{group_id}/add', ['controller' => 'MetadataTypes', 'action' => 'addModify'], ['_name' => 'metadata_types_add', 'pass' => ['project_id', 'type_id'], 'project_id' => '\d+', 'type_id' => '\d+']);
        $routes->connect('/projects/{project_id}/types/{type_id}/metadata_types/{metadata_type_id}/edit', ['controller' => 'MetadataTypes', 'action' => 'addModify'], ['_name' => 'metadata_types_edit', 'pass' => ['project_id', 'type_id', 'metadata_type_id'], 'project_id' => '\d+', 'type_id' => '\d+', 'metadata_type_id' => '\d+']);
        $routes->connect('/projects/{project_id}/types/{type_id}/metadata_types/{metadata_type_id}/delete', ['controller' => 'MetadataTypes', 'action' => 'delete'], ['_name' => 'metadata_types_delete', 'pass' => ['project_id', 'type_id', 'metadata_type_id'], 'project_id' => '\d+', 'type_id' => '\d+', 'metadata_type_id' => '\d+']);

        $routes->connect('/projects/{project_id}/types/{type_id}/relationships/{group_id}/add', ['controller' => 'TypeRelationships', 'action' => 'addModify'], ['_name' => 'type_relationships_add', 'pass' => ['project_id', 'type_id'], 'project_id' => '\d+', 'type_id' => '\d+']);
        $routes->connect('/projects/{project_id}/types/{type_id}/relationships/{type_relationship_id}/edit', ['controller' => 'TypeRelationships', 'action' => 'addModify'], ['_name' => 'type_relationships_edit', 'pass' => ['project_id', 'type_id', 'type_relationship_id'], 'project_id' => '\d+', 'type_id' => '\d+', 'type_relationship_id' => '\d+']);
        $routes->connect('/projects/{project_id}/types/{type_id}/relationships/{type_relationship_id}/delete', ['controller' => 'TypeRelationships', 'action' => 'delete'], ['_name' => 'type_relationships_delete', 'pass' => ['project_id', 'type_id', 'type_relationship_id'], 'project_id' => '\d+', 'type_id' => '\d+', 'type_relationship_id' => '\d+']);
    });

};
