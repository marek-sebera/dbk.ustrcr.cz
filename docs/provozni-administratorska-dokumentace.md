# Provozní dokumentace

### Aplikace je určena pro standardní LEMP stack (Linux, Nginx, MySQL, PHP)

Předpokládané verze komponent:
- PHP 7.4 nebo vyšší
  - vyžadované jsou komponenty mbstring, intl, json, xml, mysql, redis
- MariaDB 10.3 nebo vyšší (nebo kompatibilní)
- Nginx 1.14.2 nebo vyšší
  - konfigurace je zde v repozitáři ve složce docs/etc/nginx
  - aplikace je napsána tak, aby vynucovala transportní bezpečnost a bezpečnost prohlížeče
- Linux jádro 5.6 nebo vyšší
- Redis Server (lokální in-memory cache) 6.0 nebo vyšší
  - očekává se lokálně na portu 6379 bez autorizace/autentizace

Pro odesílání e-mailů je lokálně instalovaný Postfix, kde stačí nastavit relay,
pro odesílání skrze externí server

### Instalace

Pokud vytváříte novou instalaci, pak je potřeba
- Nginx dle sample konfigurace očekává PHP-FPM s unix socketem `/var/run/php/php7.4-fpm.sock`
- Ve vytvořeném DB Serveru, vytvořit databázi do které importujete soubor schema_with_data.sql
- V aplikaci upravit config/app_local.php
  - vepsat konfiguraci DB do klíče Datasources.default
  - vytvořit nový náhodný salt pro klíč Security.salt
  - upravit konfiguraci EmailTransport, pokud není lokálně dostupný SMTP na portu 25
- V adresáři aplikace spustit `composer install` pro instalaci závislostí

### Konfigurace aplikace

Při změně konfigurace serveru, např. přesun by nemělo být potřeba více než upravit příslušně soubor config/app_local.php
Ostatní konfigurace by měla být nezávislá na aktuálním provozním prostředí

### Konfigurace serveru

vzhledem k tomu, že je aplikace velmi nenáročná na prostředí, lze ji provozovat v aplikačním kontajneru nebo jiném
reprodukovatelném prostředí

pokud je provozováno na VPS, typicky lze povolit plnou aktualizaci všech systémových balíčků vč. těch, které se v
major verzi shodují s požadavky z předpokladů

pro účely příjmu security-patches, je aplikace konfigurována pro automatickou aktualizaci minor verzí PHP frameworku
a přímých závislostí, takže lze nastavit cron (se správnými oprávněními a dostupnou binárkou [composer](https://getcomposer.org/))
takto:
```bash
// případně změnit cestu ke composer.phar nebo kde je instalována aplikace, pokud není v /var/www/html
0 * * * * /usr/local/bin/composer update --with-all-dependencies -d /var/www/html/
```

### Poznámky k provozu

- Pokud něco nefunguje, nastavit `'debug' => true` v `app_local.php` a opravit
- Pokud je nesynchronní cache (např. s db), `redis-cli flushall`, zároveň provede odhlášení všech uživatel
- Chyby, pokud není zapnut debug, jsou v adresáři aplikace v cestě `logs/error.log` nebo `logs/debug.log`

### Zálohy a obnovení

- Aplikace sama neobsahuje žádná data, takže není potřeba zálohovat adresář aplikace
- Je třeba zálohovat kompletní DB, např. `mysqldump` bude stačit
- Při změně obsahu klíče Security.salt v `config/app_local.php` je potřeba vymazat redis
