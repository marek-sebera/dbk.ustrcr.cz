-- Pomocné datové typy mají jiné chování v UI
ALTER TABLE `types` ADD `is_helper` TINYINT(1) NOT NULL DEFAULT '0' AFTER `id`, ADD INDEX (`is_helper`);
ALTER TABLE `types` CHANGE `is_helper` `is_helper` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'je pomocný datový typ';
