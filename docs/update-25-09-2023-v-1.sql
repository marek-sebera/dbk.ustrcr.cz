-- Pevná vazba mezi všemi instancemi tohoto typu (THIS) a vázanými objekty (THEM), při vazbě THIS(1):THEM(N)
-- Umožňuje čistit databázi od orphans (sirotků) a evidenci provádět vloženým formulářem místo nutnosti
-- vytvářet vázané objekty samostatně
ALTER TABLE `types` ADD `is_linked` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_helper`, ADD INDEX (`is_linked`);
ALTER TABLE `types` CHANGE `is_linked` `is_linked` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'vázaný datový typ u kterého neprobíhá evidence samostatně';
