-- Pro účely řazení vazeb při pevně linkovaných typech
ALTER TABLE `instance_relationships` ADD `weight` INT NOT NULL DEFAULT '0' COMMENT 'Řazení vázaných objektů' AFTER `instance_to_id`;
