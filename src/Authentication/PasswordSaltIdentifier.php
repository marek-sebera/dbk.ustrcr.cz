<?php
declare(strict_types=1);

namespace App\Authentication;

use App\Model\Entity\User;
use Authentication\Identifier\PasswordIdentifier;
use Authentication\Identifier\Resolver\ResolverInterface;

class PasswordSaltIdentifier extends PasswordIdentifier
{

    protected function _checkPassword($identity, ?string $password): bool
    {
        $passwordField = $this->getConfig('fields.' . self::CREDENTIAL_PASSWORD);

        if ($identity === null) {
            $identity = [
                $passwordField => '',
            ];
        }

        $hasher = $this->getPasswordHasher();
        $hashedPassword = $identity[$passwordField];
        $passwordToCheck = (string)$password;
        if ($identity instanceof User) {
            $passwordToCheck .= $identity->salt;
        }
        if (!$hasher->check($passwordToCheck, $hashedPassword)) {
            return false;
        }

        $this->_needsPasswordRehash = $hasher->needsRehash($hashedPassword);

        return true;
    }

}
