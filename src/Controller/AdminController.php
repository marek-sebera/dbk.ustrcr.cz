<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\ProjectsTable;
use App\Model\Table\UsersTable;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Response;

class AdminController extends AppController
{

    protected UsersTable $Users;
    protected ProjectsTable $Projects;

    public function initialize(): void
    {
        parent::initialize();
        if ($this->Authentication->getIdentityData('is_superadmin') === true) {
            $this->Authorization->skipAuthorization();
        } else {
            throw new ForbiddenException();
        }

        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Users = $this->fetchTable(UsersTable::class);
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Projects = $this->fetchTable(ProjectsTable::class);
    }

    public function dashboard(): void
    {
        $users = $this->Users->find('all', [
            'contain' => [
                'Projects',
                'ProjectsToUsers',
            ],
        ]);
        $projects = $this->Projects->find('all', [
            'contain' => [
                'Users',
                'ProjectsToUsers',
                'ProjectsToUsers.Users',
            ],
        ]);

        $this->set(compact('users', 'projects'));
    }

    public function userSetEnabled(int $user_id, string $state): ?Response
    {
        $user = $this->Users->get($user_id);
        if ($this->getCurrentUserId() !== $user->id) {
            $user->is_enabled = $state === 'enabled';
            $this->Flash->success(__('Provedeno'));
        } else {
            $this->Flash->error(__('Nelze upravovat přihlášeného uživatele'));
        }
        $this->Users->save($user);
        return $this->redirect(['action' => 'dashboard']);
    }

    public function userAdminSet(int $user_id, string $state): ?Response
    {
        $user = $this->Users->get($user_id);
        if ($this->getCurrentUserId() !== $user->id) {
            $user->is_superadmin = $state === 'enabled';
            $this->Flash->success(__('Provedeno'));
        } else {
            $this->Flash->error(__('Nelze upravovat přihlášeného uživatele'));
        }
        $this->Users->save($user);
        return $this->redirect(['action' => 'dashboard']);
    }

    public function userDesignerSet(int $user_id, string $state): ?Response
    {
        $user = $this->Users->get($user_id);
        if ($this->getCurrentUserId() !== $user->id) {
            $user->is_projects_designer = $state === 'enabled';
            $this->Flash->success(__('Provedeno'));
        } else {
            $this->Flash->error(__('Nelze upravovat přihlášeného uživatele'));
        }
        $this->Users->save($user);
        return $this->redirect(['action' => 'dashboard']);
    }

    public function projectPublicSet(int $project_id, string $state): ?Response
    {
        $project = $this->Projects->get($project_id);
        $project->is_public = $state === 'enabled';
        $this->Flash->success(__('Provedeno'));
        $this->Projects->save($project);
        return $this->redirect(['action' => 'dashboard']);
    }

}
