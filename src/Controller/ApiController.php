<?php

declare(strict_types=1);


namespace App\Controller;

use App\Model\Table\ProjectsTable;
use Cake\Database\Query;
use Cake\Http\Exception\NotFoundException;

class ApiController extends AppController
{
    protected ProjectsTable $Projects;

    public function initialize(): void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(
            ['index', 'projects', 'project', 'projectType', 'projectTypeInstances'],
        );
        $this->Authorization->skipAuthorization();
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Projects = $this->fetchTable(ProjectsTable::class);
        $this->viewBuilder()->setLayout('ajax')->setClassName('Ajax');
        $this->setResponse($this->getResponse()->withType('application/json'));
    }

    public function index()
    {
        $descriptor = [
            'allowedMethods' => ['GET',],
            'actions' => [
                '/api/projects' => 'List all public projects',
                '/api/project/{id}' => 'List detail of public project with types and',
                '/api/project/{id}/type/{type-id}' => 'Detailed info about data type properties and relationships',
                '/api/project/{id}/type/{type-id}/instances' => 'List all instances of type within project',
                '/api/project/{id}/all' => 'Project with all instances',
            ],
        ];

        return $this->getResponse()->withStringBody(json_encode($descriptor));
    }

    public function projects()
    {
        return $this->getResponse()->withStringBody(json_encode($this->Projects->find('public')->jsonSerialize()));
    }

    public function project(?int $id = null)
    {
        if (empty($id) || $id < 1) {
            throw new NotFoundException();
        }
        $data = $this->Projects
            ->find('public')
            ->where(['Projects.id' => $id])
            ->contain([
                'Types' => function (Query $q) {
                    return $q->select(['id', 'type_name', 'modified', 'project_id']);
                },
            ])
            ->firstOrFail();
        return $this->getResponse()->withStringBody(json_encode($data));
    }

    public function projectType(?int $project_id = null, ?int $type_id = null)
    {
        if (empty($project_id) || $project_id < 1 || empty($type_id) || $type_id < 1) {
            return $this->getResponse()->withStatus(404);
        }
        $project = $this->Projects
            ->find('public')
            ->where(['Projects.id' => $project_id])
            ->firstOrFail();

        $data = $this->Projects->Types->get($type_id, [
            'conditions' => [
                'Types.project_id' => $project->id,
            ],
            'contain' => [
                'MetadataTypes',
                'MetadataTypes.DataTypes',
                'MetadataTypes.TypePropertiesGroups',
            ],
        ]);

        return $this->getResponse()->withStringBody(json_encode($data));
    }

    public function projectTypeInstances(?int $project_id = null, ?int $type_id = null)
    {
        if (empty($project_id) || $project_id < 1 || empty($type_id) || $type_id < 1) {
            return $this->getResponse()->withStatus(404);
        }
        $project = $this->Projects
            ->find('public')
            ->where(['Projects.id' => $project_id])
            ->firstOrFail();

        $data = $this->Projects->Instances->find('all', [
            'conditions' => [
                'Instances.project_id' => $project->id,
                'Instances.type_id' => $type_id,
            ],
            'contain' => [
                'Metadata',
                'Metadata.MetadataTypes',
                'Metadata.MetadataTypes.DataTypes',
            ],
        ]);

        return $this->getResponse()->withStringBody(json_encode($data));
    }

}
