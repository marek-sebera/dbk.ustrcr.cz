<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\User;
use ArrayAccess;
use Authentication\Controller\Component\AuthenticationComponent;
use Authorization\Controller\Component\AuthorizationComponent;
use Cake\Controller\Controller;
use Cake\ORM\Table;
use Closure;
use ReflectionException;
use ReflectionFunction;

/**
 * @property AuthenticationComponent $Authentication
 * @property AuthorizationComponent $Authorization
 */
class AppController extends Controller
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('FormProtection');
        $this->loadComponent('Authentication.Authentication');
        $this->loadComponent('Authorization.Authorization');
    }

    public function allToList($data, Table $table)
    {
        $rtn = [];
        $idField = $table->getPrimaryKey();
        $valueField = $table->getDisplayField();
        foreach ($data as $item) {
            $id = is_array($item) ? $item[$idField] : $item->{$idField};
            $value = is_array($item) ? $item[$valueField] : $item->{$valueField};
            $rtn[$id] = $value;
        }
        return $rtn;
    }

    public function getCurrentUserId(): int
    {
        if (!$this->Authentication || !$this->Authentication->getIdentity()) {
            return 0;
        }
        return intval($this->Authentication->getIdentity()->getIdentifier());
    }

    /**
     * @return null|User|ArrayAccess|array
     */
    public function getCurrentUser(): ?ArrayAccess
    {
        if (!$this->Authentication || !$this->Authentication->getIdentity()) {
            return null;
        }
        return $this->Authentication->getIdentity()->getOriginalData();
    }

    /**
     * @throws ReflectionException
     */
    public function invokeAction(Closure $action, array $args): void
    {
        $reflection = new ReflectionFunction($action);
        $passedArgs = [];
        $counter = 0;
        foreach ($reflection->getParameters() as $reflectionParameter) {
            if (!$reflectionParameter->hasType()) {
                $passedArgs[] = $args[$counter];
            } else {
                switch ($reflectionParameter->getType()->getName()) {
                    default:
                        dump(__METHOD__, $reflectionParameter->getType()->getName());
                        die();
                    case "int":
                        if (!isset($args[$counter]) && $reflectionParameter->allowsNull()) {
                            $passedArgs[] = null;
                        } else {
                            $passedArgs[] = isset($args[$counter]) ? intval($args[$counter]) : 0;
                        }
                        break;
                    case "string":
                        if (!isset($args[$counter]) && $reflectionParameter->allowsNull()) {
                            $passedArgs[] = null;
                        } else {
                            $passedArgs[] = isset($args[$counter]) ? strval($args[$counter]) : "";
                        }
                }
            }
            $counter++;
        }

        parent::invokeAction($action, $passedArgs);
    }
}
