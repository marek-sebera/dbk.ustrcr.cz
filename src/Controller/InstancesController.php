<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Instance;
use App\Model\Entity\Project;
use App\Model\Table\InstancesTable;
use App\Model\Table\MetadataTypesTable;
use App\Model\Table\ProjectsTable;

class InstancesController extends AppController
{

    protected ProjectsTable $Projects;
    protected InstancesTable $Instances;

    public function initialize(): void
    {
        parent::initialize();
        $this->Authorization->skipAuthorization();
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Projects = $this->fetchTable(ProjectsTable::class);
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Instances = $this->fetchTable(InstancesTable::class);
    }

    public function delete(int $project_id, int $type_id, int $instance_id): void
    {
        /** @var Project $project */
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->contain(ProjectsTable::ALL_CONTAINS)->firstOrFail();
        /** @var Instance $instance */
        $instance = $this->Instances->getInstance($this->Authentication->getIdentity(), $project->id, $type_id, $instance_id)->firstOrFail();
        if (!$project->userCanDelete($this->getCurrentUserId())) {
            $this->Flash->error(__('Nemáte oprávnění mazat'));
        } else {
            $toDelete = [$instance];
            foreach ($instance->type->types_relationships as $tr) {
                if ($tr->to_type->is_linked) {
                    $toDelete = array_merge($toDelete, $instance->getCurrentRelationshipsByType($tr));
                }
            }
            if ($this->Instances->deleteMany($toDelete)) {
                $this->Flash->success(__('Smazáno'));
            } else {
                $this->Flash->error(__('Nelze smazat'));
            }
        }

        $this->redirect(['_name' => 'project_detail', 'id' => $project->id]);
    }

    public function detail(int $project_id, int $type_id, int $instance_id): void
    {
        /** @var Project $project */
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->contain(ProjectsTable::ALL_CONTAINS)->firstOrFail();
        /** @var Instance $instance */
        $instance = $this->Instances->getInstance($this->Authentication->getIdentity(), $project->id, $type_id, $instance_id)->contain(InstancesTable::ALL_CONTAIN)->firstOrFail();
        $instance->type = $this->Instances->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->firstOrFail();

        $metadata_types = $this->Instances->Types->MetadataTypes->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->contain(MetadataTypesTable::ALL_CONTAINS)->all();

        $types_relationships = $this->Instances->Types->TypesRelationships->find('all', [
            'conditions' => [
                'TypesRelationships.from_type_id' => $type_id
            ]
        ]);

        $reverse_relationships = $this->Instances->InstanceReverseRelationships->find('all', [
            'conditions' => [
                'InstanceReverseRelationships.instance_to_id' => $instance->id
            ],
            'contain' => [
                'InstanceFrom',
                'TypesRelationships'
            ]
        ])->order(['InstanceReverseRelationships.types_relationship_id' => 'ASC'])->toArray();

        $properties_groups = $this->Instances->Types->TypePropertiesGroups->find('all', [
            'conditions' => [
                'TypePropertiesGroups.type_id' => $instance->type_id
            ],
            'contain' => [
                'MetadataTypes',
                'TypesRelationships'
            ]
        ]);

        $this->set(compact('project', 'instance', 'properties_groups', 'reverse_relationships', 'metadata_types', 'types_relationships'));
        $this->set('crumbs', [__('Projekty') => 'projects', $project->name => ['_name' => 'project_detail', 'id' => $project->id], $instance->type->type_name => '#']);
    }

    public function addModify(int $project_id, int $type_id, ?int $instance_id = null): void
    {
        /** @var Project $project */
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->contain(ProjectsTable::ALL_CONTAINS)->firstOrFail();
        /** @var Instance $instance */
        $instance = $instance_id > 0 ? $this->Instances->getInstance($this->Authentication->getIdentity(), $project->id, $type_id, $instance_id)->contain(InstancesTable::ALL_CONTAIN)->firstOrFail()
            : $this->Instances->newEntity([
                'project_id' => $project_id,
                'type_id' => $type_id,
                'name' => ''
            ]);
        $instance->type = $this->Instances->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->firstOrFail();

        $types = $this->Projects->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id)->toArray();
        $typesIndexed = [];
        $typesList = [];
        $relevant_type_ids = [$type_id];
        foreach ($types as $type) {
            $relevant_type_ids[] = $type->id;
            $typesList[$type->id] = $this->Instances->find('list')->where([
                'Instances.type_id' => $type->id,
                'Instances.project_id' => $project_id
            ]);
            $typesIndexed[$type->id] = $type;
        }

        $metadata_types = [];
        foreach ($relevant_type_ids as $relevant_type_id) {
            $metadata_types[$relevant_type_id] = $this->Instances->Types->MetadataTypes->getTypeByProject($this->Authentication->getIdentity(), $project_id, $relevant_type_id)->contain(MetadataTypesTable::ALL_CONTAINS)->all();
        }

        $types_relationships = $this->Instances->Types->TypesRelationships->find('all', [
            'conditions' => [
                'TypesRelationships.from_type_id' => $type_id
            ],
            'contain' => [
                'ToTypes'
            ]
        ]);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            // do basic merge of current DB instance and edited fields (metadata+name)
            $instance = $this->Instances->patchEntity(
                $instance,
                $this->getRequest()->getData(), ['associated' => ['Metadata', 'InstanceRelationships']]
            );
            // check non-linked relationships to delete
            $relationshipsToDelete = $instance->updateRelationships(
                $this->Instances,
                $this->getRequest()->getData()
            );

            // type of instance is never changed, this is needed just because of "type" property assignment above
            $instance->setDirty('type', false);

            if ($project->userCanEdit($this->getCurrentUserId()) && $this->Instances->save($instance)) {
                $this->Instances->InstanceRelationships->deleteManyOrFail($relationshipsToDelete);

                $this->Flash->success(__('Uloženo'));
                $this->redirect(['action' => 'detail', 'project_id' => $project_id, 'type_id' => $type_id, 'instance_id' => $instance->id]);
            } else {
                if ($project->userCanEdit($this->getCurrentUserId())) {
                    $this->Flash->error(__('Formulář obsahuje chyby'));
                } else {
                    $this->Flash->error(__('Nemáte oprávnění upravovat data'));
                }
            }
        }

        $this->set(compact('instance', 'types', 'typesIndexed', 'typesList', 'project', 'metadata_types', 'types_relationships'));
        $this->set('crumbs', [__('Projekty') => 'projects', $project->name => ['_name' => 'project_detail', 'id' => $project->id], $instance->type->type_name => ['_name' => 'project_detail', 'id' => $project->id, '#' => 'tab-' . $instance->type_id]]);
    }
}
