<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Project;
use App\Model\Table\MetadataTypesTable;
use App\Model\Table\ProjectsTable;

class MetadataTypesController extends AppController
{
    protected MetadataTypesTable $MetadataTypes;
    protected ProjectsTable $Projects;

    public function initialize(): void
    {
        parent::initialize();
        $this->Authorization->skipAuthorization();

        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Projects = $this->fetchTable(ProjectsTable::class);
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->MetadataTypes = $this->fetchTable(MetadataTypesTable::class);
    }

    public function addModify(int $project_id, int $type_id, ?int $metadata_type_id = null): void
    {
        /** @var Project $project */
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->firstOrFail();
        $type = $metadata_type_id > 0 ? $this->MetadataTypes->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id, $metadata_type_id)->firstOrFail() : $this->MetadataTypes->newEntity([
            'project_id' => $project_id,
            'type_id' => $type_id,
            'type_properties_group_id' => $this->getRequest()->getParam('group_id'),
        ]);
        $type->type = $this->MetadataTypes->Types->get($type->type_id, [
            'conditions' => ['Types.project_id' => $project_id],
            'contain' => ['TypePropertiesGroups']
        ]);
        $metadata_type = $this->MetadataTypes->Types->get($type_id);
        $dataTypes = $this->MetadataTypes->DataTypes->find('list');

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $type = $this->MetadataTypes->patchEntity($type, $this->getRequest()->getData());
            if (!$project->userCanEdit($this->getCurrentUserId())) {
                $this->Flash->error(__('Nemáte oprávnění upravovat'));
            } else {
                if ($type->isNew() && empty($type->type_properties_group) && empty($type->type_properties_group_id)) {
                    $group = $this->MetadataTypes->TypePropertiesGroups->newEntity([
                        'group_name' => __('Nezařazeno'),
                        'type_id' => $type_id
                    ]);
                    $this->MetadataTypes->TypePropertiesGroups->save($group);
                    $type->type_properties_group = $group;
                }
                if ($this->MetadataTypes->save($type)) {
                    $this->Flash->success(__('Uloženo'));
                    $this->redirect(['_name' => 'project_detail', 'id' => $project_id, '#' => 'tab-'.$type_id]);
                } else {
                    $this->Flash->error(__('Formulář obsahuje chyby'));
                }
            }
        }

        $this->set(compact('type', 'project', 'dataTypes'));
        $this->set('crumbs', [
                __('Projekty') => 'projects',
                $project->name => ['_name' => 'project_detail', 'id' => $project->id],
                $metadata_type->type_name => ['_name' => 'project_detail', 'id' => $project->id, '#' => 'tab-' . $metadata_type->id]]
        );
    }

    public function delete(int $project_id, int $type_id, int $metadata_type_id): void
    {

        /** @var Project $project */
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->firstOrFail();
        $type = $this->MetadataTypes->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id, $metadata_type_id)->firstOrFail();
        if ($project->userCanDelete($this->getCurrentUserId())) {
            if ($this->MetadataTypes->delete($type)) {
                $this->Flash->success(__('Smazáno'));
            } else {
                $this->Flash->error(__('Nelze smazat'));
            }
        } else {
            $this->Flash->error(__('Nemáte oprávnění mazat'));
        }
        $this->redirect(['_name' => 'project_detail', 'id' => $project->id, '#' => 'tab-' . $type->id]);
    }

}
