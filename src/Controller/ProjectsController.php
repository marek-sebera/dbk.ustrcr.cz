<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\DataType;
use App\Model\Entity\Project;
use App\Model\Entity\RelationshipType;
use App\Model\Table\ProjectsTable;
use App\Model\Table\UsersTable;

class ProjectsController extends AppController
{

    protected ProjectsTable $Projects;
    protected UsersTable $Users;

    public function initialize(): void
    {
        parent::initialize();
        $this->Authorization->skipAuthorization();
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Projects = $this->fetchTable(ProjectsTable::class);
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Users = $this->fetchTable(UsersTable::class);
    }

    public function index(): void
    {
        $projects = $this->Projects->getProjectsByUser($this->Authentication->getIdentity())->contain(ProjectsTable::ALL_CONTAINS)->toArray();

        $this->set(compact('projects'));
    }

    public function addModify(int $id = 0): void
    {
        if ($this->getCurrentUser()->is_superadmin === true && $id > 0) {
            $project = $this->Projects->get($id, [
                'contain' => [
                    'ProjectsToUsers',
                    'ProjectsToUsers.Users'
                ]
            ]);
        } else {
            $project = $id > 0 ? $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $id)->firstOrFail() : $this->Projects->newEmptyEntity();
        }
        $usersList = $this->Projects->Users->find('list', [
            'conditions' => [
                'Users.id !=' => intval($project->user_id)
            ]
        ]);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $success_message = __('Uloženo úspěšně');
            if ($project->isNew()) {
                if ($this->getCurrentUser()->is_projects_designer) {
                    $project->user_id = $this->Authentication->getIdentity()->getIdentifier();
                    $success_message = __('Projekt vytvořen');
                }
            }
            $project = $this->Projects->patchEntity($project, $this->getRequest()->getData());
            // if empty share (no user to share with) created automatically, remove it before saving
            foreach ($project->projects_to_users ?? [] as $key => $share) {
                if (empty(intval($share->user_id))) {
                    if (!$share->isNew()) {
                        $this->Projects->ProjectsToUsers->delete($share);
                    }
                    unset($project->projects_to_users[$key]);
                }
            }

            if ($this->getCurrentUser()->is_superadmin !== true && !$project->userCanEdit($this->getCurrentUserId())) {
                $this->Flash->error(__('Nemáte právo k úpravám'));
            } else if ($this->Projects->save($project)) {
                $this->Flash->success($success_message);
                $this->redirect($this->getRequest()->getRequestTarget());
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('project', 'usersList'));
        $this->set('crumbs', [__('Projekty') => 'projects', $project->name => ['_name' => 'project_detail', 'id' => $project->id]]);
    }

    public function addExample(): void
    {
        $this->redirect(['action' => 'index']);
        if (!$this->getCurrentUser()->is_projects_designer) {
            $this->Flash->error(__('Nemáte oprávnění vytvářet projekty'));
            return;
        }
        $this->Flash->error(__('Vytvoření se nezdařilo'));

        // single project
        $project = $this->Projects->newEntity([
            'user_id' => $this->Authentication->getIdentity()->getIdentifier(),
            'name' => 'Ukázkový projekt'
        ]);
        $this->Projects->saveOrFail($project);

        // types
        $person = $this->Projects->Types->newEntity(['type_name' => 'Osoba', 'project_id' => $project->id, 'type_properties_groups' => [['group_name' => __('Nezařazeno')]]]);
        $place = $this->Projects->Types->newEntity(['type_name' => 'Místo', 'project_id' => $project->id, 'type_properties_groups' => [['group_name' => __('Nezařazeno')]]]);
        $document = $this->Projects->Types->newEntity(['type_name' => 'Dokument', 'project_id' => $project->id, 'type_properties_groups' => [['group_name' => __('Nezařazeno')]]]);
        $event = $this->Projects->Types->newEntity(['type_name' => 'Událost', 'project_id' => $project->id, 'type_properties_groups' => [['group_name' => __('Nezařazeno')]]]);
        $this->Projects->Types->saveManyOrFail([$person, $place, $document, $event]);

        // types metadata
        $person_name = $this->Projects->Types->MetadataTypes->newEntity(['type_name' => 'Celé jméno', 'type_id' => $person->id, 'type_properties_group_id' => $person->type_properties_groups[0]->id, 'data_type_id' => DataType::TYPE_SHORT_TEXT, 'project_id' => $project->id]);
        $place_description = $this->Projects->Types->MetadataTypes->newEntity(['type_name' => 'Popis', 'type_id' => $place->id, 'type_properties_group_id' => $place->type_properties_groups[0]->id, 'data_type_id' => DataType::TYPE_LONG_TEXT, 'project_id' => $project->id]);
        $event_when = $this->Projects->Types->MetadataTypes->newEntity(['type_name' => 'Datum', 'type_id' => $event->id, 'type_properties_group_id' => $event->type_properties_groups[0]->id, 'data_type_id' => DataType::TYPE_DATE, 'project_id' => $project->id]);
        $document_type = $this->Projects->Types->MetadataTypes->newEntity(['type_name' => 'Typ dokumentu', 'type_id' => $document->id, 'type_properties_group_id' => $document->type_properties_groups[0]->id, 'data_type_id' => DataType::TYPE_SHORT_TEXT, 'project_id' => $project->id]);
        $person_id = $this->Projects->Types->MetadataTypes->newEntity(['type_name' => 'Evidenční číslo', 'type_id' => $person->id, 'type_properties_group_id' => $person->type_properties_groups[0]->id, 'data_type_id' => DataType::TYPE_INTEGER, 'project_id' => $project->id]);
        $this->Projects->Types->MetadataTypes->saveManyOrFail([$person_name, $person_id, $place_description, $event_when, $document_type]);

        // types relations / links - types
        $person_parent = $this->Projects->Types->TypesRelationships->newEntity([
            'relationship_name' => 'Rodič',
            'from_type_id' => $person->id,
            'to_type_id' => $person->id,
            'relationship_type_id' => RelationshipType::TYPE_BELONGS_TO,
            'type_properties_group_id' => $person->type_properties_groups[0]->id,
        ]);
        $person_place_of_birth = $this->Projects->Types->TypesRelationships->newEntity([
            'relationship_name' => 'Místo narození',
            'from_type_id' => $person->id,
            'to_type_id' => $place->id,
            'relationship_type_id' => RelationshipType::TYPE_BELONGS_TO,
            'type_properties_group_id' => $person->type_properties_groups[0]->id,
        ]);
        $place_partners = $this->Projects->Types->TypesRelationships->newEntity([
            'relationship_name' => 'Partnerská města',
            'from_type_id' => $place->id,
            'to_type_id' => $place->id,
            'relationship_type_id' => RelationshipType::TYPE_HAS_MANY,
            'type_properties_group_id' => $place->type_properties_groups[0]->id,
        ]);
        $person_documents = $this->Projects->Types->TypesRelationships->newEntity([
            'relationship_name' => 'Osobní doklady',
            'from_type_id' => $person->id,
            'to_type_id' => $document->id,
            'relationship_type_id' => RelationshipType::TYPE_HAS_MANY,
            'type_properties_group_id' => $person->type_properties_groups[0]->id,
        ]);
        $person_death = $this->Projects->Types->TypesRelationships->newEntity([
            'relationship_name' => 'Úmrtí',
            'from_type_id' => $person->id,
            'to_type_id' => $event->id,
            'relationship_type_id' => RelationshipType::TYPE_BELONGS_TO,
            'type_properties_group_id' => $person->type_properties_groups[0]->id,
        ]);
        $document_location = $this->Projects->Types->TypesRelationships->newEntity([
            'relationship_name' => 'Umístění originálu',
            'from_type_id' => $document->id,
            'to_type_id' => $place->id,
            'relationship_type_id' => RelationshipType::TYPE_BELONGS_TO,
            'type_properties_group_id' => $document->type_properties_groups[0]->id,
        ]);
        $this->Projects->Types->TypesRelationships->saveManyOrFail([
            $person_parent, $person_death, $person_place_of_birth, $place_partners, $person_documents, $document_location
        ]);

        // instances
        $person_a = $this->Projects->Instances->newEntity([
            'type_id' => $person->id,
            'project_id' => $project->id,
            'name' => 'Osoba 1',
            'metadata' => [
                ['metadata_type_id' => $person_name->id, 'text_value' => 'První Osoba']
            ]
        ]);
        $person_b = $this->Projects->Instances->newEntity([
            'type_id' => $person->id,
            'project_id' => $project->id,
            'name' => 'Osoba 2',
            'metadata' => [
                ['metadata_type_id' => $person_name->id, 'text_value' => 'Druhá Osoba'],
                ['metadata_type_id' => $person_id->id, 'number_value' => '121314']
            ]
        ]);
        $document_a = $this->Projects->Instances->newEntity([
            'type_id' => $document->id,
            'project_id' => $project->id,
            'name' => 'Dokument 1',
            'metadata' => [
                ['metadata_type_id' => $document_type->id, 'text_value' => 'Kopie listin v archivu ÚSTR']
            ]
        ]);
        $event_a = $this->Projects->Instances->newEntity([
            'type_id' => $event->id,
            'project_id' => $project->id,
            'name' => 'Událost 1',
            'metadata' => [
                ['metadata_type_id' => $event_when->id, 'datetime_value' => '2000-03-05 00:00:00']
            ]
        ]);
        $place_a = $this->Projects->Instances->newEntity([
            'type_id' => $place->id,
            'project_id' => $project->id,
            'name' => 'Místo 1',
            'metadata' => [
                ['metadata_type_id' => $place_description->id, 'text_value' => 'Dlouhý popis místa může obsahovat až 65.535 znaků']
            ]
        ]);
        $this->Projects->Instances->saveManyOrFail([$person_a, $person_b, $document_a, $event_a, $place_a]);

        // links between instances
        $person_death_link = $this->Projects->Instances->InstanceRelationships->newEntity([
            'types_relationship_id' => $person_death->id,
            'instance_from_id' => $person_a->id,
            'instance_to_id' => $event_a->id
        ]);
        $person_parent_link = $this->Projects->Instances->InstanceRelationships->newEntity([
            'types_relationship_id' => $person_parent->id,
            'instance_from_id' => $person_b->id,
            'instance_to_id' => $person_a->id
        ]);
        $person_place_of_birth_link = $this->Projects->Instances->InstanceRelationships->newEntity([
            'types_relationship_id' => $person_place_of_birth->id,
            'instance_from_id' => $person_b->id,
            'instance_to_id' => $place_a->id
        ]);

        $this->Projects->Instances->InstanceRelationships->saveManyOrFail([
            $person_death_link, $person_parent_link, $person_place_of_birth_link
        ]);

        $this->Flash->success(__('Vytvořeno'), ['clear' => true]);
        $this->redirect(['action' => 'detail', 'id' => $project->id]);
    }

    public function detail(int $id): void
    {
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $id)
            ->contain(ProjectsTable::ALL_CONTAINS)
            ->firstOrFail();

        $this->set(compact('project'));
        $this->set('crumbs', [__('Projekty') => 'projects']);
    }

    public function projectStructure(int $id): void
    {
        /** @var Project $project */
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $id)
            ->contain(ProjectsTable::ALL_CONTAINS)
            ->firstOrFail();

        $countByType = [];
        foreach ($project->types as $type) {
            $countByType[$type->id] = $this->Projects->Instances->find('list', [
                'conditions' => [
                    'Instances.project_id' => $id,
                    'Instances.type_id' => $type->id
                ]
            ])->count();
        }
        $dataTypes = $this->Projects->MetadataTypes->DataTypes->find('list')->toArray();

        $this->set(compact('project', 'countByType', 'dataTypes'));
        $this->set('crumbs', [__('Projekty') => 'projects', h($this->viewBuilder()->getVar('project')->name) => ['action' => 'detail', 'id' => $id]]);
    }

    public function delete(int $id): void
    {
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $id)->firstOrFail();
        if ($project->user_id !== $this->getCurrentUserId()) {
            $this->Flash->error(__('Pouze vlastník projektu jej může smazat'));
        } else if ($this->Projects->delete($project)) {
            $this->Flash->success(__('Smazáno'));
        } else {
            $this->Flash->error(__('Nebylo možné smazat'));
        }
        $this->redirect(['action' => 'index']);
    }

}
