<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Response;

class PublicController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index']);
        $this->Authorization->skipAuthorization();
    }

    public function index(): void
    {
        if ($this->Authentication->getResult()) {
            $this->redirect(['_name' => 'projects']);
        }
    }

}
