<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Project;
use App\Model\Entity\Type;
use App\Model\Table\TypesTable;

class TypePropertiesGroupsController extends AppController
{
    protected TypesTable $Types;

    public function initialize(): void
    {
        parent::initialize();
        $this->Authorization->skipAuthorization();
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Types = $this->fetchTable(TypesTable::class);
    }

    public function addModify(int $project_id, int $type_id, ?int $group_id = null): void
    {
        $type = $this->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->firstOrFail();
        /** @var Project $project */
        $project = $this->Types->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->firstOrFail();
        $group = $group_id > 0 ? $this->Types->TypePropertiesGroups->get($group_id, [
            'conditions' => [
                'TypePropertiesGroups.type_id' => $type->id
            ]
        ]) : $this->Types->TypePropertiesGroups->newEntity([
            'type_id' => $type->id
        ]);

        if ($project->userCanEdit($this->getCurrentUserId()) && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $group = $this->Types->TypePropertiesGroups->patchEntity($group, $this->getRequest()->getData());
            if ($this->Types->TypePropertiesGroups->save($group)) {
                $this->Flash->success(__('Uloženo'));
                $this->redirect(['_name' => 'project_detail', 'id' => $project_id, '#' => 'tab-'.$type->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('type', 'group'));
        $this->set('crumbs', [__('Projekty') => 'projects', $project->name => ['_name' => 'project_detail', 'id' => $project->id], $type->type_name => '#']);
    }

    public function delete(int $project_id, int $type_id, int $group_id): void
    {
        /** @var Type $type */
        $type = $this->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->firstOrFail();
        $group = $this->Types->TypePropertiesGroups->get($group_id, [
            'conditions' => [
                'TypePropertiesGroups.type_id' => $type->id
            ]
        ]);
        if ($type->project->userCanDelete($this->getCurrentUserId())) {
            if($this->Types->TypePropertiesGroups->delete($group)) {
                $this->Flash->success(__('Smazáno'));
            } else {
                $this->Flash->error(__('Nastala chyba při mazání'));
            }
        } else {
            if (!$type->project->userCanDelete($this->getCurrentUserId())) {
                $this->Flash->error(__('Nemáte oprávnění mazat'));
            } else {
                $this->Flash->error(__('Nebylo možné smazat skupinu vlastností'));
            }
        }
        $this->redirect(['_name' => 'project_detail', 'id' => $project_id, '#' => 'tab-'.$type_id]);
    }

}
