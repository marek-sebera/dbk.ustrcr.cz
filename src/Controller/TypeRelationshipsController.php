<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Project;
use App\Model\Table\ProjectsTable;
use App\Model\Table\TypesRelationshipsTable;
use App\Model\Table\TypesTable;

class TypeRelationshipsController extends AppController
{
    protected TypesRelationshipsTable $TypesRelationships;
    protected ProjectsTable $Projects;
    protected TypesTable $Types;

    public function initialize(): void
    {
        parent::initialize();
        $this->Authorization->skipAuthorization();
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->TypesRelationships = $this->fetchTable(TypesRelationshipsTable::class);
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Projects = $this->fetchTable(ProjectsTable::class);
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Types = $this->fetchTable(TypesTable::class);
    }

    public function addModify(int $project_id, int $type_id, ?int $type_relationship_id = null): void
    {
        /** @var Project $project */
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->firstOrFail();
        $fromType = $this->Projects->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->firstOrFail();
        $type = $type_relationship_id > 0 ? $this->TypesRelationships->getRelationshipByProject($this->Authentication->getIdentity(), $type_id, $type_relationship_id)->firstOrFail() : $this->TypesRelationships->newEntity([
            'project_id' => $project_id,
            'from_type_id' => $type_id,
            'type_properties_group_id' => $this->getRequest()->getParam('group_id'),
        ]);
        $relationshipTypes = $this->TypesRelationships->RelationshipTypes->find('list');
        $types = $this->allToList($this->Projects->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id), $this->Types);
        $instance_listing = [];
        foreach($types as $type_id => $type_name){
            $instance_listing[$type_id] = $this->Projects->Instances->find('list', [
                'conditions' => [
                    'Instances.project_id' => $project_id,
                    'Instances.type_id' => $type_id
                ]
            ])->toArray();
        }

        if (!$project->userCanEdit($this->getCurrentUserId())) {
            $this->Flash->error(__('Nemáte oprávnění upravovat'));
        }
        elseif ($project->userCanEdit($this->getCurrentUserId()) && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $type = $this->TypesRelationships->patchEntity($type, $this->getRequest()->getData());
            if ($type->isNew() && empty($type->type_properties_group) && empty($type->type_properties_group_id)) {
                $group = $this->TypesRelationships->TypePropertiesGroups->newEntity([
                    'group_name' => __('Nezařazeno'),
                    'type_id' => $type_id
                ]);
                $this->TypesRelationships->TypePropertiesGroups->save($group);
                $type->type_properties_group = $group;
            }
            if ($this->TypesRelationships->save($type)) {
                $this->Flash->success(__('Uloženo'));
                $this->redirect(['_name' => 'project_detail', 'id' => $project_id, '#' => 'tab-' . $fromType->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('type', 'project', 'relationshipTypes', 'fromType', 'types', 'instance_listing'));
        $this->set('crumbs', [
            __('Projekty') => 'projects',
            $project->name => ['_name' => 'project_detail', 'id' => $project->id],
            $fromType->type_name => ['_name' => 'project_detail', 'id' => $project->id, '#' => 'tab-' . $fromType->id]]
        );
    }

    public function delete(int $project_id, int $type_id, int $type_relationship_id): void
    {
        /** @var Project $project */
        $project = $this->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->firstOrFail();
        $type = $this->TypesRelationships->getRelationshipByProject($this->Authentication->getIdentity(), $type_id, $type_relationship_id)->firstOrFail();

        if ($project->userCanDelete($this->getCurrentUserId())) {
            if ($this->TypesRelationships->delete($type)) {
                $this->Flash->success(__('Smazáno'));
            } else {
                $this->Flash->error(__('Nelze smazat'));
            }
        } else {
            $this->Flash->error(__('Nemáte oprávnění mazat'));
        }
        $this->redirect(['_name' => 'project_detail', 'id' => $project->id]);
    }
}
