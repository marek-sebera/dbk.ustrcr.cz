<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\TypesTable;
class TypesController extends AppController
{
    protected TypesTable $Types;

    public function initialize(): void
    {
        parent::initialize();
        $this->Authorization->skipAuthorization();
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Types = $this->fetchTable(TypesTable::class);
    }

    public function addModify(int $project_id, ?int $type_id = null): void
    {
        $type = $type_id > 0 ? $this->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->firstOrFail() : $this->Types->newEntity([
            'project_id' => $project_id
        ]);
        $metadata_list = $type_id > 0 ? $this->Types->MetadataTypes->find('list', ['conditions' => [
            'MetadataTypes.type_id' => $type_id,
            'MetadataTypes.project_id' => $project_id
        ]]) : [];
        $relationships_list = $type_id > 0 ? $this->Types->TypesRelationships->find('list', ['conditions' => [
            'TypesRelationships.from_type_id' => $type_id
        ]]) : [];
        $project = $this->Types->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->firstOrFail();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $type = $this->Types->patchEntity($type, $this->getRequest()->getData());
            if ($type->isNew()) {
                $type->type_properties_groups = [
                    $this->Types->TypePropertiesGroups->newEntity([
                        'group_name' => __('Nezařazeno')
                    ])
                ];
            }
            if ($this->Types->save($type)) {
                $this->Flash->success(__('Uloženo'));
                $this->redirect(['_name' => 'project_detail', 'id' => $project_id, '#' => 'tab-'.$type->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('type', 'metadata_list', 'relationships_list'));
        $this->set('crumbs', [
            __('Projekty') => 'projects',
            $project->name => ['_name' => 'project_detail', 'id' => $project->id],
            $type->type_name => ['_name' => 'project_detail', 'id' => $project->id, '#' => 'tab-'. $type->id]
        ]);
    }

    public function delete(int $project_id, int $type_id): void
    {
        $type = $this->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->firstOrFail();
        if ($this->Types->delete($type)) {
            $this->Flash->success(__('Smazáno'));
        } else {
            $this->Flash->error(__('Nebylo možné smazat datový typ'));
        }
        $this->redirect(['_name' => 'project_detail', 'id' => $project_id]);
    }

    public function allInstances(int $project_id, int $type_id)
    {
        $type = $this->Types->getTypeByProject($this->Authentication->getIdentity(), $project_id, $type_id)->contain(TypesTable::ALL_CONTAIN)->firstOrFail();
        $project = $this->Types->Projects->getProjectsByUser($this->Authentication->getIdentity(), $project_id)->firstOrFail();
        $instances = $this->Types->Instances->find('all', [
            'conditions' => [
                'Instances.type_id' => $type_id,
                'Instances.project_id' => $project_id,
            ],
            'contain' => [
                'Metadata',
                'InstanceRelationships',
                'InstanceRelationships.InstanceTo'
            ]
        ])->toArray();

        $this->set(compact('type', 'instances'));
        $this->set('crumbs', [__('Projekty') => 'projects', $project->name => ['_name' => 'project_detail', 'id' => $project->id], $type->type_name => ['_name' => 'types_edit', 'project_id' => $project_id, 'type_id' => $type_id]]);
    }

}
