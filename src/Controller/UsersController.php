<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Cake\Http\Response;
use Cake\Mailer\Mailer;
use Cake\Routing\Router;

class UsersController extends AppController
{
    protected UsersTable $Users;

    public function initialize(): void
    {
        parent::initialize();
        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->Users = $this->fetchTable(UsersTable::class);
        $this->Authentication->allowUnauthenticated(['login', 'register', 'passwordReset', 'passwordResetGo']);
        $this->Authorization->skipAuthorization();
    }

    public function register(): void
    {
        $user = $this->Users->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $entityData = $this->getRequest()->getData();
            $entityData['salt'] = random_str('alphanum', 64);
            $user = $this->Users->newEntity($entityData);
            $this->set(compact('user'));

            if (!endsWith($user->email, '@ustrcr.cz')) {
                $this->Flash->error('Registrovat se mohou pouze uživatelé s e-mailovou adresou v doméně @ustrcr.cz');
                return;
            }

            if ($user->hasErrors()) {
                return;
            }

            $user->is_enabled = true;
            $user->password = password_hash($user->password . $user->salt, PASSWORD_ARGON2ID);

            if ($this->Users->save($user)) {
                $this->Flash->success('Registrace proběhla úspěšně, nyní se můžete přihlásit');
                $this->redirect(['_name' => 'login']);
            } else {
                $this->Flash->error('Registrace neproběhla úspěšně, prosím zkuste to znovu');
            }
        }

        $this->set(compact('user'));
    }

    public function login(): ?Response
    {
        $result = $this->Authentication->getResult();

        if (!$this->getRequest()->is(['post', 'put', 'patch']) && $this->getCurrentUser() instanceof User) {
            return $this->redirect('/');
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $user = $result->getData();

            if (!$result->isValid() || !($user instanceof User)) {
                $this->Flash->error('Neplatné uživatelské jméno nebo heslo');

                return null;
            }

            if ($user->is_enabled !== true) {
                $this->Flash->error(__('Váš účet je aktuálně zakázán'));
                $this->Authentication->logout();

                return null;
            }

            return $this->redirect($this->Authentication->getLoginRedirect() ?? '/');
        }
        $this->set('user', $this->Users->newEmptyEntity());
        return null;
    }

    public function logout(): ?Response
    {
        $this->Authentication->logout();
        return $this->redirect('/');
    }

    public function passwordReset(): void
    {
        $this->set('user', $this->Users->newEmptyEntity());
        if ($this->Authentication->getResult()->isValid()) {
            $this->redirect('/');
            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            /** @var User $user_by_email */
            $user_by_email = $this->Users->find('all', [
                'conditions' => [
                    'Users.email' => $this->getRequest()->getData('email'),
                ],
            ])->first();

            $this->Flash->success(__('Pokračujte prosím otevřením odkazu v e-mailu, který jsme vám právě zaslali'));
            $this->redirect('/');
            if (empty($user_by_email)) {
                return;
            }

            $user_by_email->regenerateEmailToken();
            $this->Users->save($user_by_email);

            $mailer = new Mailer();
            $mailer
                ->setFrom(['system@dbk.ustrcr.cz' => 'Databáze K'])
                ->setTo($user_by_email->email)
                ->setSubject(__('Odkaz k obnovení hesla'))
                ->deliver(
                    Router::url(
                        [
                            '_name' => 'password_recovery_go',
                            'token' => $user_by_email->mail_verification_code,
                            'requestId' => $user_by_email->id,
                            '_full' => true,
                        ],
                    ),
                );
        }
    }

    public function passwordResetGo(): void
    {
        if ($this->Authentication->getResult()->isValid()) {
            $this->redirect('/');
            return;
        }

        $user = $this->Users->get($this->getRequest()->getParam('requestId'), [
            'conditions' => [
                'Users.mail_verification_code' => $this->getRequest()->getParam('token'),
            ],
        ]);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if (!empty($this->getRequest()->getData('password'))) {
                $user->regenerateSalt();
                $user->regenerateEmailToken();
                $user->setNewPassword($this->getRequest()->getData('password'));

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Heslo bylo změněno úspěšně. Nyní se můžete přihlásit.'));
                    $this->redirect(['_name' => 'login']);
                } else {
                    $this->Flash->error(__('Chyba při ukládání'));
                }
            }
        }

        $this->set(compact('user'));
    }

}
