<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DataType Entity
 *
 * @property int $id
 * @property string $type_name
 *
 * @property MetadataType[] $metadata_types
 */
class DataType extends Entity
{
    public const TYPE_SHORT_TEXT = 1, TYPE_LONG_TEXT = 2, TYPE_DATE = 3, TYPE_DATETIME = 4, TYPE_INTEGER = 5, TYPE_FLOAT = 6, TYPE_CHECKBOX = 7;
    public const TYPES_TO_DEFAULT_VALUE_FORM_CONTROL_TYPE = [
        self::TYPE_LONG_TEXT => 'string',
        self::TYPE_SHORT_TEXT => 'string',
        self::TYPE_DATE => 'date',
        self::TYPE_DATETIME => 'datetime-local',
        self::TYPE_INTEGER => ['type' => 'number', 'step' => 1],
        self::TYPE_FLOAT => ['type' => 'number', 'step' => 0.01],
        self::TYPE_CHECKBOX => ['type' => 'checkbox']
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_name' => true,
        'metadata_types' => true,
    ];

    public static function dataTableColumnType(int $data_type_id): string
    {
        switch ($data_type_id) {
            default:
            case self::TYPE_CHECKBOX:
            case self::TYPE_LONG_TEXT:
            case self::TYPE_SHORT_TEXT:
                return 'string';
            case self::TYPE_DATE:
            case self::TYPE_DATETIME:
                return 'date';
            case self::TYPE_INTEGER:
            case self::TYPE_FLOAT:
                return 'num';
        }
    }

    public static function fromString(?string $default_value, int $data_type_id)
    {
        if ($default_value === null || $default_value === '') {
            return null;
        }
        switch ($data_type_id) {
            case self::TYPE_CHECKBOX:
                return intval($default_value) === 1;
            default:
            case self::TYPE_LONG_TEXT:
            case self::TYPE_SHORT_TEXT:
                return $default_value;
            case self::TYPE_DATE:
                return \DateTime::createFromFormat('Y-m-d', $default_value);
            case self::TYPE_DATETIME:
                return \DateTime::createFromFormat('Y-m-d H:i:s', $default_value);
            case self::TYPE_INTEGER:
                return intval($default_value);
            case self::TYPE_FLOAT:
                return floatval($default_value);
        }
    }
}
