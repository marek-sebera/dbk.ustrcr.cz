<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Table\InstancesTable;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Instance Entity
 *
 * @property int $id
 * @property int $project_id
 * @property int $type_id
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property string $name
 *
 * @property Project $project
 * @property Type $type
 * @property Metadata[] $metadata
 * @property InstanceRelationship[] $instance_relationships
 * @property InstanceRelationship[] $instance_reverse_relationships
 */
class Instance extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'type_id' => true,
        'modified' => true,
        'created' => true,
        'name' => true,
        'project' => true,
        'type' => true,
        'metadata' => true,
        'instance_relationships' => true,
    ];

    public function getMetadataValueOrNull(MetadataType $metadataType, bool $humanReadable = false)
    {
        list($index, $metadata) = $this->getMetadataByType($metadataType);
        return $index == -1 ? null : $metadata->getValueByType($humanReadable);
    }

    /**
     * @param MetadataType $metadataType
     * @return array [int, Metadata]
     */
    public function getMetadataByType(MetadataType $metadataType): array
    {
        foreach ($this->metadata ?? [] as $index => $metadata) {
            if ($metadata->metadata_type_id === $metadataType->id) {
                return [$index, $metadata];
            }
        }
        return [-1, null];
    }

    /**
     * @param TypesRelationship $typesRelationship
     * @param bool $orderByWeight
     * @return int[]
     */
    public function getCurrentRelationshipIdsByType(
        TypesRelationship $typesRelationship,
        bool              $orderByWeight = false
    ): array
    {
        $relationships = $this->getCurrentRelationshipsByType($typesRelationship);
        if ($orderByWeight) {
            usort($relationships, function (InstanceRelationship $a, InstanceRelationship $b) {
                return $b->weight - $a->weight;
            });
        }
        return array_map(function ($relation) {
            return $relation->instance_to_id;
        }, $relationships);
    }

    /**
     * @param TypesRelationship $typesRelationship
     * @return InstanceRelationship[]
     */
    public function getCurrentRelationshipsByType(TypesRelationship $typesRelationship): array
    {
        $instance_tos = [];
        foreach ($this->instance_relationships ?? [] as $relationship) {
            if ($relationship->types_relationship_id === $typesRelationship->id) {
                $instance_tos[] = $relationship;
            }
        }
        return $instance_tos;
    }

    /**
     * Updates internal field instance_relationships, returns array of relationships to delete
     *
     * @param InstancesTable $instancesTable
     * @param array|null $data
     * @return InstanceRelationship[]|array
     */
    public function updateRelationships(InstancesTable $instancesTable, ?array $data): array
    {
        $updated = [];
        $delete = [];
        $instanceRelationshipsTable = $instancesTable->InstanceRelationships;
        if (empty($data['relation'])) {
            return [];
        }
        foreach ($data['relation'] as $types_relationship_id => $selectedValue) {
            $dummy_types_relationship = new TypesRelationship(['id' => $types_relationship_id]);
            if (empty($selectedValue) || (is_array($selectedValue) && count($selectedValue) === 1 && empty($selectedValue[0]))) {
                // delete all belongs-to or has-many relations
                foreach ($this->getCurrentRelationshipsByType($dummy_types_relationship) as $relToDel) {
                    $delete[$relToDel->id] = $relToDel;
                }
                continue;
            }
            if (is_array($selectedValue)) {
                $existing = $this->getCurrentRelationshipsByType($dummy_types_relationship);
                $existing_to_instance_ids = [];
                foreach ($existing as $existing_relation) {
                    $existing_to_instance_ids[] = $existing_relation->instance_to_id;
                    if (in_array($existing_relation->instance_to_id, $selectedValue)) {
                        $updated[] = $existing_relation;
                    } else {
                        $delete[$existing_relation->id] = $existing_relation;
                    }
                }
                foreach ($selectedValue as $selectedInstanceToId) {
                    if (!in_array($selectedInstanceToId, $existing_to_instance_ids)) {
                        $updated[] = $instanceRelationshipsTable->newEntity([
                            'types_relationship_id' => $types_relationship_id,
                            'instance_from_id' => $this->id,
                            'instance_to_id' => $selectedInstanceToId
                        ]);
                    }
                }
                continue;
            }
            // will reach here if value is selected, not empty, and is not multi-select type
            $existing = $this->getCurrentRelationshipsByType($dummy_types_relationship);
            for ($c = 1; $c < count($existing); $c++) {
                // if more than one found, delete others
                $delete[$existing[$c]->id] = $existing[$c];
            }
            $existing = count($existing) > 0 ? $existing[0] : $instanceRelationshipsTable->newEntity([
                'types_relationship_id' => $types_relationship_id,
                'instance_from_id' => $this->id,
                'instance_to_id' => intval($selectedValue)
            ]);
            // to be sure it's correctly set
            if ($existing->instance_to_id != intval($selectedValue)) {
                $existing->set('instance_to_id', intval($selectedValue));
            }
            $updated[] = $existing;
        }

        list($linkedMaintained, $linkedDeleted) = $this->patchLinked($instancesTable, $data['linked'] ?? []);

        if ((count($linkedDeleted) + count($linkedMaintained) + count($updated) + count($delete)) < count($this->instance_relationships ?? [])) {
            throw new NotFoundException(
                'counts dont add up not even to original relationships count, something is very wrong'
            );
        }

        $this->instance_relationships = array_merge($linkedMaintained, $updated);
        $this->setDirty('instance_relationships');
        return array_values(array_merge($delete, $linkedDeleted));
    }

    /**
     * @param InstancesTable $instancesTable
     * @param array $linkedFormData
     * @return array [maintained InstanceRelationship[], deleted InstanceRelationship[]]
     */
    function patchLinked(InstancesTable $instancesTable, array $linkedFormData): array
    {
        $deleted_relationships = [];
        $maintained_relationships = [];

        foreach ($linkedFormData as $typesRelationshipsId => $instances) {
            $typesRelationship = $this->type->getTypesRelationshipById($typesRelationshipsId);
            $instanceFromId = $this->id;
            $currentTypeRelationships = $this->getCurrentRelationshipsByType($typesRelationship);
            foreach ($instances as $linked_instance_data) {
                $is_linked = boolval($linked_instance_data['_is_linked'] ?? false);
                $instance_id = intval($linked_instance_data['id'] ?? 0);
                $has_id = $instance_id > 0;
                $current_relationship = $has_id ? $this->getCurrentRelationshipByTypeAndInstanceId($typesRelationship, $instance_id) : null;

                if (!$is_linked && $has_id) {
                    // delete existing
                    $deleted_relationships[$current_relationship->id] = $current_relationship;
                } else if ($is_linked && !$has_id) {
                    // create new
                    $new_instance = $instancesTable->newEntity($linked_instance_data + [
                            'project_id' => $this->project_id,
                            'type_id' => $typesRelationship->to_type_id,
                        ]);
                    $new_relation = $instancesTable->InstanceRelationships->newEntity([
                        'types_relationship_id' => $typesRelationshipsId,
                        'instance_from_id' => $instanceFromId,
                        'types_relationship' => null,
                        'weight' => true
                    ]);
                    $new_relation->set('instance_to', $new_instance);
                    $maintained_relationships[] = $new_relation;
                } else if ($is_linked && $has_id) {
                    $instancesTable->loadInto($current_relationship->instance_to, InstancesTable::ALL_CONTAIN);
                    $instancesTable->patchEntity($current_relationship->instance_to, $linked_instance_data);
                    $current_relationship->instance_to->updateRelationships(
                        $instancesTable,
                        $linked_instance_data
                    );
                    if ($current_relationship->instance_to->isDirty()) {
                        $current_relationship->setDirty('instance_to');
                    }
                    $maintained_relationships[] = $current_relationship;
                }
            }
        }

        return [$maintained_relationships, $deleted_relationships];
    }

    function getCurrentRelationshipByTypeAndInstanceId(
        TypesRelationship $relationship,
        int               $related_instance_id,
        bool              $throwIfNotFound = true
    ): ?InstanceRelationship
    {
        foreach ($this->getCurrentRelationshipsByType($relationship) as $_rel) {
            if ($_rel->instance_to_id == $related_instance_id) {
                return $_rel;
            }
        }
        if ($throwIfNotFound) {
            throw new NotFoundException(
                sprintf("getCurrentRelationshipByTypeAndInstanceId failed for [relationship %d, instance %d]",
                    $relationship->id, $related_instance_id
                ));
        }
        return null;
    }
}
