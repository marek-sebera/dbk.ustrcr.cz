<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InstanceRelationship Entity
 *
 * @property int $id
 * @property int $types_relationship_id
 * @property int $instance_from_id
 * @property int $instance_to_id
 * @property int $weight
 *
 * @property TypesRelationship $types_relationship
 * @property Instance $instance_to
 * @property Instance $instance_from
 */
class InstanceRelationship extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'types_relationship_id' => true,
        'instance_from_id' => true,
        'instance_to_id' => true,
        'types_relationship' => true,
        'instance_to' => true,
        'instance_from' => true,
        'weight' => true
    ];
}
