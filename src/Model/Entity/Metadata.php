<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\I18n\I18n;
use Cake\I18n\Number;
use Cake\ORM\Entity;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;

/**
 * Metadata Entity
 *
 * @property int $id
 * @property int $object_id
 * @property int $metadata_type_id
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property float|null $number_value
 * @property FrozenTime|null $datetime_value
 * @property string|null $text_value
 * @property bool|null $boolean_value
 *
 * @property Object $object
 * @property MetadataType $metadata_type
 * @property Type[] $types
 */
class Metadata extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'object_id' => true,
        'metadata_type_id' => true,
        'modified' => true,
        'created' => true,
        'number_value' => true,
        'datetime_value' => true,
        'text_value' => true,
        'boolean_value' => true,
        'object' => true,
        'metadata_type' => true,
        'types' => true,
    ];

    public function getValueByType(bool $humanReadable = false): ?string
    {
        $data_type_id = $this->metadata_type ? $this->metadata_type->data_type_id : null;
        if ($data_type_id === null) {
            $data_type_id = TableRegistry::getTableLocator()->get('MetadataTypes')->get($this->metadata_type_id)->data_type_id;
        }
        switch ($data_type_id) {
            default:
                dump(__METHOD__, $this, $data_type_id);
                die();
            case DataType::TYPE_LONG_TEXT:
            case DataType::TYPE_SHORT_TEXT:
                return empty($this->text_value) ? null : $this->text_value;
            case DataType::TYPE_DATE:
                return $this->datetime_value ? ($humanReadable ? $this->datetime_value->i18nFormat('d.M.y') : $this->datetime_value->toDateString()) : null;
            case DataType::TYPE_DATETIME:
                return $this->datetime_value ? ($humanReadable ? $this->datetime_value->i18nFormat('d.M.y H:i:s') : $this->datetime_value->toDateTimeString()) : null;
            case DataType::TYPE_INTEGER:
                return $this->number_value === null ? null : strval(round($this->number_value, 0));
            case DataType::TYPE_FLOAT:
                return strval($this->number_value);
            case DataType::TYPE_CHECKBOX:
                return $humanReadable ? ($this->number_value ? __('Ano') : __('Ne')) : strval(boolval($this->number_value));
        }
    }
}
