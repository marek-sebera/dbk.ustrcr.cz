<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\View\Helper\FormHelper;
use Cake\ORM\Entity;

/**
 * MetadataType Entity
 *
 * @property int $id
 * @property string $type_name
 * @property int $type_id
 * @property int $data_type_id
 * @property int $project_id
 * @property int $type_properties_group_id
 * @property null|string $default_value
 *
 * @property Type $type
 * @property DataType $data_type
 * @property Project $project
 * @property Metadata[] $metadata
 * @property TypePropertiesGroup $type_properties_group
 */
class MetadataType extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_name' => true,
        'type_id' => true,
        'data_type_id' => true,
        'project_id' => true,
        'type' => true,
        'data_type' => true,
        'project' => true,
        'metadata' => true,
        'type_properties_group' => true,
        'type_properties_group_id' => true,
        'default_value' => true
    ];

    /**
     * @param FormHelper $form
     * @param int $counter
     * @param MetadataType $metadataType
     * @param Instance $instance
     * @param string $controlPrefix
     * @return string
     */
    public static function createFormControl(
        FormHelper $form,
        int $counter,
        MetadataType $metadataType,
        Instance $instance,
        string $controlPrefix = '',
    ): string
    {
        list($index, $metadata) = $instance->getMetadataByType($metadataType);
        $counter = $index === -1 ? $counter : $index;

        $typeControl = $form->control($controlPrefix . sprintf('metadata.%d.metadata_type_id', $counter), ['type' => 'hidden', 'value' => $metadataType->id]);
        $valueControlConfig = [
            'type' => $metadataType->getFormControlType(),
            'default' => $metadataType->getDefaultValue(),
            'label' => $metadataType->type_name
        ];
        $value = $instance->getMetadataValueOrNull($metadataType);
        if ($value !== null) {
            $valueKey = $valueControlConfig['type'] === 'checkbox' ? 'checked' : 'value';
            $value = $valueControlConfig['type'] === 'checkbox' ? boolval($value) : $value;
            $valueControlConfig[$valueKey] = $value;
        }
        $valueControl = $form->control($controlPrefix . sprintf('metadata.%d.%s', $counter, $metadataType->getValueColumnName()), $valueControlConfig + $metadataType->getFormControlExtras());
        $idControl = "";
        if (!empty($metadata)) {
            $idControl = $form->control($controlPrefix . sprintf('metadata.%d.id', $counter), ['value' => $metadata->id]);
        }
        return $typeControl . $idControl . $valueControl;
    }

    public function getValueColumnName()
    {
        switch ($this->data_type_id) {
            case DataType::TYPE_SHORT_TEXT:
            case DataType::TYPE_LONG_TEXT:
                return 'text_value';
            case DataType::TYPE_DATE:
            case DataType::TYPE_DATETIME:
                return 'datetime_value';
            case DataType::TYPE_FLOAT:
            case DataType::TYPE_INTEGER:
            case DataType::TYPE_CHECKBOX:
                return 'number_value';
            default:
                dump(__METHOD__, $this);
                die();
        }
    }

    private function getFormControlType()
    {
        switch ($this->data_type_id) {
            case DataType::TYPE_SHORT_TEXT:
                return 'string';
            case DataType::TYPE_LONG_TEXT:
                return 'textarea';
            case DataType::TYPE_DATE:
                return 'date';
            case DataType::TYPE_DATETIME:
                return 'datetime';
            case DataType::TYPE_FLOAT:
            case DataType::TYPE_INTEGER:
                return 'number';
            case DataType::TYPE_CHECKBOX:
                return 'checkbox';
            default:
                dump(__METHOD__, $this);
                die();
        }
    }

    private function getFormControlExtras(): array
    {
        switch ($this->data_type_id) {
            case DataType::TYPE_SHORT_TEXT:
                return ['maxlength' => 255];
            case DataType::TYPE_LONG_TEXT:
                return ['maxlength' => 65535];
            case DataType::TYPE_DATE:
            case DataType::TYPE_CHECKBOX:
                return [];
            case DataType::TYPE_DATETIME:
                return ['step' => 60];
            case DataType::TYPE_FLOAT:
                return ['step' => 0.00000001];
            case DataType::TYPE_INTEGER:
                return ['step' => 1, 'pattern' => '[0-9]', 'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57', 'empty' => true];
            default:
                dump(__METHOD__, $this);
                die();
        }
    }

    private function getDefaultValue()
    {
        return $this->default_value === null ? null : DataType::fromString($this->default_value, $this->data_type_id);
    }
}
