<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Table\TypesTable;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Exception;

/**
 * Project Entity
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property boolean $is_public
 * @property FrozenTime|null $modified
 *
 * @property User $user
 * @property Instance[] $instances
 * @property MetadataType[] $metadata_types
 * @property Type[] $types
 * @property ProjectsToUser[] $projects_to_users
 * @property User[] $shared_to_users
 */
class Project extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'user_id' => true,
        'modified' => true,
        'user' => true,
        'instances' => true,
        'metadata_types' => true,
        'types' => true,
        'projects_to_users' => true,
        'shared_to_users' => true,
        'is_public' => true
    ];

    public function userCanEdit(int $user_id): bool
    {
        if ($this->user_id === $user_id) {
            return true;
        }
        if (!isset($this->projects_to_users) || empty($this->projects_to_users)) {
            return false;
        }
        foreach ($this->projects_to_users as $link) {
            if ($link->user_id === $user_id) {
                if ($link->is_read_only) {
                    return false;
                }
                return true;
            }
        }
        // fallback always disable edit/view
        return false;
    }

    public function userCanDelete(int $user_id): bool
    {
        if (!$this->userCanEdit($user_id)) {
            return false;
        }
        foreach ($this->projects_to_users as $link) {
            if ($link->user_id === $user_id) {
                return $link->is_delete_enabled;
            }
        }
        // fallback always disable delete
        return false;
    }

    /**
     * Sort types so that first created will remain the first displayed, others are ordered by name alphabetically
     *
     * @return array|Type[]|null[]
     * @throws Exception
     */
    public function orderedTypes(string $types = TypesTable::ALL_TYPES): array {
        if (count($this->types) < 1) {
            return [];
        }
        if (!in_array($types, TypesTable::VALID_TYPES)) {
            throw new Exception("Invalid usage, type not valid {$types}");
        }
        $filtered = $this->types;
        if ($types != TypesTable::ALL_TYPES) {
            $filtered = array_filter($filtered, function(Type $type) use ($types) {
                return $type->is_helper == ($types == TypesTable::HELPER_TYPES);
            });
        }
        usort($filtered, function(Type $a, Type $b) {
           return strcmp($a->type_name, $b->type_name);
        });
        return $filtered;
    }
}
