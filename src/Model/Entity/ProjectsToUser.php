<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProjectsToUser Entity
 *
 * @property int $id
 * @property int $project_id
 * @property int $user_id
 * @property bool $is_read_only
 * @property bool $is_delete_enabled
 * @property bool $is_design_enabled
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\User $user
 */
class ProjectsToUser extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'user_id' => true,
        'is_read_only' => true,
        'is_delete_enabled' => true,
        'is_design_enabled' => true,
        'modified' => true,
        'project' => true,
        'user' => true,
    ];
}
