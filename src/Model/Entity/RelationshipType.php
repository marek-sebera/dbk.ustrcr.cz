<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RelationshipType Entity
 *
 * @property int $id
 * @property string $name
 * @property null|string $description
 *
 * @property \App\Model\Entity\TypesRelationship[] $types_relationships
 */
class RelationshipType extends Entity
{
    public const TYPE_BELONGS_TO = 1, TYPE_HAS_MANY = 2;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'types_relationships' => true,
        'description' => true
    ];

    protected function _getLabel(): string
    {

        return empty($this->description) ? $this->name : sprintf("%s (%s)", $this->name, $this->description);
    }
}
