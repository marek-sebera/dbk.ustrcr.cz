<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Table\TypePropertiesGroupsTable;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Locator\TableLocator;

/**
 * Type Entity
 *
 * @property int $id
 * @property int $project_id
 * @property string $type_name
 * @property bool $is_helper is helper data type, not displayed on the front of project detail
 * @property bool $is_linked is linked data type, evidenced through inlined form sections and hard-linked to avoid leaving orphans
 * @property FrozenTime|null $created
 * @property FrozenTime|null $modified
 *
 * @property Project $project
 * @property Instance[] $instances
 * @property Metadata[] $metadata
 * @property MetadataType[] $metadata_types
 * @property TypesRelationship[] $types_relationships
 * @property TypePropertiesGroup[] $type_properties_groups
 * @property MetadataType[] $shown_metadata
 * @property TypesRelationship[] $shown_relationships
 */
class Type extends Entity
{
    use LocatorAwareTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'is_helper' => true,
        'is_linked' => true,
        'project_id' => true,
        'type_name' => true,
        'created' => true,
        'modified' => true,
        'project' => true,
        'instances' => true,
        'metadata' => true,
        'metadata_types' => true,
        'types_relationships' => true,
        'type_properties_groups' => true,
        'shown_metadata' => true,
        'shown_relationships' => true
    ];

    /**
     * @param int $id
     * @param bool $throwIfNotFound
     * @return TypesRelationship|null
     */
    public function getTypesRelationshipById(int $id, bool $throwIfNotFound = true): ?TypesRelationship
    {
        if ($this->types_relationships === null) {
            $this->getTableLocator()->get('Types')->loadInto($this, ['TypesRelationships']);
        }
        foreach ($this->types_relationships ?? [] as $types_relationship) {
            if ($types_relationship->id === $id) {
                return $types_relationship;
            }
        }
        if ($throwIfNotFound) {
            throw new NotFoundException(sprintf("getTypesRelationshipById failed to found %d in instance %d", $id, $this->id));
        }
        return null;
    }
}
