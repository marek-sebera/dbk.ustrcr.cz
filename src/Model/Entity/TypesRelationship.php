<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\View\Helper\FormHelper;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * TypesRelationship Entity
 *
 * @property int $id
 * @property string $relationship_name
 * @property int $from_type_id
 * @property int $to_type_id
 * @property int $relationship_type_id
 * @property int $type_properties_group_id
 * @property int $default_value
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Type $from_type
 * @property Type $to_type
 * @property RelationshipType $relationship_type
 * @property TypePropertiesGroup $type_properties_group
 */
class TypesRelationship extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'relationship_name' => true,
        'from_type_id' => true,
        'to_type_id' => true,
        'relationship_type_id' => true,
        'modified' => true,
        'created' => true,
        'from_type' => true,
        'to_type' => true,
        'relationship_type' => true,
        'type_properties_group_id' => true,
        'type_properties_group' => true,
        'default_value' => true
    ];

    public static function createFormControl(
        FormHelper        $form,
        TypesRelationship $relationship,
        Instance          $instanceFrom,
        array             $typesList,
        string            $controlPrefix = '',
        array             $formControlOptions = [],
    ): string
    {
        $currentValue = $instanceFrom->getCurrentRelationshipIdsByType($relationship);
        if (!empty($currentValue)) {
            $currentValue = ['value' => count($currentValue) === 1 ? $currentValue[0] : $currentValue];
        }

        // add missing dot for prefix
        if (mb_strlen($controlPrefix) && !endsWith($controlPrefix, ".")) {
            $controlPrefix .= ".";
        }

        return $form->control($controlPrefix . 'relation.' . $relationship->id, [
                'label' => $relationship->relationship_name,
                'type' => 'select',
                'options' => $typesList[$relationship->to_type_id],
                'default' => empty($relationship->default_value) ? false : $relationship->default_value,
                'multiple' => $relationship->relationship_type_id === RelationshipType::TYPE_HAS_MANY,
                'empty' => __('Vyberte si z nabízených možností nebo ponechte prázdné'),
            ] + $currentValue + $formControlOptions);
    }
}
