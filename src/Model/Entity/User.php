<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Authentication\IdentityInterface;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $salt
 * @property string|null $mail_verification_code
 * @property bool $is_enabled
 * @property bool $is_superadmin
 * @property bool $is_projects_designer
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 */
class User extends Entity implements IdentityInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'password' => true,
        'salt' => true,
        'mail_verification_code' => true,
        'is_enabled' => true,
        'is_superadmin' => true,
        'modified' => true,
        'created' => true,
        'is_projects_designer' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    public function getIdentifier()
    {
        return $this->id;
    }

    public function getOriginalData()
    {
        return $this;
    }

    public function regenerateEmailToken(bool $setEmpty = false)
    {
        $this->mail_verification_code = $setEmpty ? null : random_str('alphanum', 32);
        $this->setDirty('mail_verification_code');
    }

    public function regenerateSalt()
    {
        $this->salt = random_str('alphanum', 64);
        $this->setDirty('salt');
    }

    public function setNewPassword(string $newPassword)
    {
        while (empty($this->salt)) {
            $this->regenerateSalt();
        }
        $this->password = password_hash($newPassword . $this->salt, PASSWORD_ARGON2ID);
        $this->setDirty('password');
    }
}
