<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\DataType;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DataTypes Model
 *
 * @property MetadataTypesTable&HasMany $MetadataTypes
 *
 * @method DataType newEmptyEntity()
 * @method DataType newEntity(array $data, array $options = [])
 * @method DataType[] newEntities(array $data, array $options = [])
 * @method DataType get($primaryKey, $options = [])
 * @method DataType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method DataType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method DataType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method DataType|false save(EntityInterface $entity, $options = [])
 * @method DataType saveOrFail(EntityInterface $entity, $options = [])
 * @method DataType[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method DataType[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method DataType[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method DataType[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class DataTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('data_types');
        $this->setDisplayField('type_name');
        $this->setPrimaryKey('id');

        $this->hasMany('MetadataTypes', [
            'foreignKey' => 'data_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        return $validator;
    }
}
