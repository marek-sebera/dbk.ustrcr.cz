<?php

declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\InstanceRelationship;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InstanceRelationships Model
 *
 * @property TypesRelationshipsTable&BelongsTo $TypesRelationships
 * @property InstancesTable&BelongsTo $InstanceTo
 * @property InstancesTable&BelongsTo $InstanceFrom
 *
 * @method InstanceRelationship newEmptyEntity()
 * @method InstanceRelationship newEntity(array $data, array $options = [])
 * @method InstanceRelationship[] newEntities(array $data, array $options = [])
 * @method InstanceRelationship get($primaryKey, $options = [])
 * @method InstanceRelationship findOrCreate($search, ?callable $callback = null, $options = [])
 * @method InstanceRelationship patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method InstanceRelationship[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method InstanceRelationship|false save(EntityInterface $entity, $options = [])
 * @method InstanceRelationship saveOrFail(EntityInterface $entity, $options = [])
 * @method InstanceRelationship[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method InstanceRelationship[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method InstanceRelationship[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method InstanceRelationship[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class InstanceRelationshipsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('instance_relationships');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TypesRelationships', [
            'foreignKey' => 'types_relationship_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('InstanceFrom', [
            'foreignKey' => 'instance_from_id',
            'joinType' => 'INNER',
            'className' => 'Instances',
        ]);
        $this->belongsTo('InstanceTo', [
            'foreignKey' => 'instance_to_id',
            'joinType' => 'INNER',
            'className' => 'Instances',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['types_relationship_id'], 'TypesRelationships'));
        $rules->add($rules->existsIn(['instance_from_id'], 'InstanceFrom'));
        $rules->add($rules->existsIn(['instance_to_id'], 'InstanceTo'));

        return $rules;
    }

    public function beforeSave(EventInterface $event, EntityInterface $entity, ArrayObject $options)
    {
        // add default value before saving
        if ($entity->isNew()) {
            if (empty($entity->get('weight'))) {
                // default value for weight
                $entity->set('weight', 0);
            }
        }
    }
}
