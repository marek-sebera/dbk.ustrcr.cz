<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Instance;
use Authentication\IdentityInterface;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Instances Model
 *
 * @property ProjectsTable&BelongsTo $Projects
 * @property TypesTable&BelongsTo $Types
 * @property MetadataTable&HasMany $Metadata
 * @property InstanceRelationshipsTable&HasMany $InstanceRelationships
 * @property InstanceRelationshipsTable&HasMany $InstanceReverseRelationships
 *
 * @method Instance newEmptyEntity()
 * @method Instance newEntity(array $data, array $options = [])
 * @method Instance[] newEntities(array $data, array $options = [])
 * @method Instance get($primaryKey, $options = [])
 * @method Instance findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Instance patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Instance[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Instance|false save(EntityInterface $entity, $options = [])
 * @method Instance saveOrFail(EntityInterface $entity, $options = [])
 * @method Instance[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Instance[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Instance[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Instance[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class InstancesTable extends Table
{
    public const ALL_CONTAIN = [
        'Types',
        'Metadata',
        'Metadata.MetadataTypes',
        'Metadata.MetadataTypes.DataTypes',
        'Projects',
        'InstanceRelationships',
        'InstanceRelationships.InstanceTo'
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('instances');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Types', [
            'foreignKey' => 'type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Metadata', [
            'foreignKey' => 'object_id'
        ]);
        $this->hasMany('InstanceRelationships', [
            'foreignKey' => 'instance_from_id'
        ]);
        $this->hasMany('InstanceReverseRelationships',[
            'foreignKey'=>'instance_to_id',
            'className'=>'InstanceRelationships'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['project_id'], 'Projects'));
        $rules->add($rules->existsIn(['type_id'], 'Types'));

        return $rules;
    }

    public function getInstance(?IdentityInterface $identity, $project_id, int $type_id, ?int $instance_id): Query
    {
        $query = $this->find('all')->where(['Instances.project_id' => $project_id, 'Instances.type_id' => $type_id]);
        if ($instance_id > 0) {
            $query->where(['Instances.id' => $instance_id]);
        }
        return $query;
    }
}
