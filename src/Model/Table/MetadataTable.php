<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\MetadataType;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Metadata Model
 *
 * @property \App\Model\Table\ObjectsTable&\Cake\ORM\Association\BelongsTo $Objects
 * @property \App\Model\Table\MetadataTypesTable&\Cake\ORM\Association\BelongsTo $MetadataTypes
 * @property \App\Model\Table\TypesTable&\Cake\ORM\Association\BelongsToMany $Types
 *
 * @method \App\Model\Entity\Metadata newEmptyEntity()
 * @method \App\Model\Entity\Metadata newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Metadata[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Metadata get($primaryKey, $options = [])
 * @method \App\Model\Entity\Metadata findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Metadata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Metadata[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Metadata|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Metadata saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Metadata[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Metadata[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Metadata[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Metadata[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MetadataTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('metadata');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Instances', [
            'foreignKey' => 'object_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('MetadataTypes', [
            'foreignKey' => 'metadata_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Types', [
            'foreignKey' => 'metadata_id',
            'targetForeignKey' => 'type_id',
            'joinTable' => 'metadata_types',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('number_value')
            ->allowEmptyString('number_value');

        $validator
            ->scalar('text_value')
            ->allowEmptyString('text_value');

        $validator
            ->boolean('boolean_value')
            ->allowEmptyString('boolean_value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['object_id'], 'Instances'));
        $rules->add($rules->existsIn(['metadata_type_id'], 'MetadataTypes'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (isset($data['datetime_value']) && mb_strlen($data['datetime_value']) <= 10) {
            $data['datetime_value'] .= ' 00:00:00';
        }
    }
}
