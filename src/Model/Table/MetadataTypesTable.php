<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\MetadataType;
use Authentication\IdentityInterface;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MetadataTypes Model
 *
 * @property TypesTable&BelongsTo $Types
 * @property DataTypesTable&BelongsTo $DataTypes
 * @property ProjectsTable&BelongsTo $Projects
 * @property MetadataTable&HasMany $Metadata
 * @property TypePropertiesGroupsTable&BelongsTo $TypePropertiesGroups
 *
 * @method MetadataType newEmptyEntity()
 * @method MetadataType newEntity(array $data, array $options = [])
 * @method MetadataType[] newEntities(array $data, array $options = [])
 * @method MetadataType get($primaryKey, $options = [])
 * @method MetadataType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method MetadataType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method MetadataType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method MetadataType|false save(EntityInterface $entity, $options = [])
 * @method MetadataType saveOrFail(EntityInterface $entity, $options = [])
 * @method MetadataType[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method MetadataType[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method MetadataType[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method MetadataType[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MetadataTypesTable extends Table
{
    public const ALL_CONTAINS = [
        'Types',
        'DataTypes',
        'Projects',
        'Metadata',
        'TypePropertiesGroups'
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('metadata_types');
        $this->setDisplayField('type_name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Types', [
            'foreignKey' => 'type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('DataTypes', [
            'foreignKey' => 'data_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Metadata', [
            'foreignKey' => 'metadata_type_id',
        ]);
        $this->belongsTo('TypePropertiesGroups');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['type_id'], 'Types'));
        $rules->add($rules->existsIn(['data_type_id'], 'DataTypes'));
        $rules->add($rules->existsIn(['project_id'], 'Projects'));

        return $rules;
    }

    public function getTypeByProject(IdentityInterface $getIdentity, int $project_id, int $type_id, ?int $metadata_type_id = null): Query
    {
        $query = $this->find('all')->where(['MetadataTypes.project_id' => $project_id, 'MetadataTypes.type_id' => $type_id]);
        if ($metadata_type_id > 0) {
            $query->where(['MetadataTypes.id' => $metadata_type_id]);
        }
        return $query;
    }
}
