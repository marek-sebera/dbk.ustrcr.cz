<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Project;
use ArrayObject;
use Authentication\IdentityInterface;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Projects Model
 *
 * @property UsersTable&BelongsTo $Users
 * @property InstancesTable&HasMany $Instances
 * @property MetadataTypesTable&HasMany $MetadataTypes
 * @property TypesTable&HasMany $Types
 * @property ProjectsToUsersTable&HasMany $ProjectsToUsers
 *
 * @method Project newEmptyEntity()
 * @method Project newEntity(array $data, array $options = [])
 * @method Project[] newEntities(array $data, array $options = [])
 * @method Project get($primaryKey, $options = [])
 * @method Project findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Project patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Project[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Project|false save(EntityInterface $entity, $options = [])
 * @method Project saveOrFail(EntityInterface $entity, $options = [])
 * @method Project[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Project[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Project[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Project[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectsTable extends Table
{
    public const ALL_CONTAINS = [
        'Users',
        'ProjectsToUsers',
        'Types',
        'Types.MetadataTypes',
        'Types.TypePropertiesGroups',
        'Types.Instances',
        'Types.Instances.Metadata',
        'Types.Instances.InstanceRelationships',
        'Types.Instances.InstanceRelationships.InstanceTo',
        'Types.ShownMetadata',
        'Types.ShownRelationships',
        'Types.TypesRelationships',
        'Types.TypesRelationships.FromTypes',
        'Types.TypesRelationships.ToTypes',
        'Types.TypesRelationships.RelationshipTypes',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('projects');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Instances', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('MetadataTypes', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('Types', [
            'foreignKey' => 'project_id',
        ]);
        $this->belongsToMany('SharedToUsers', [
            'through' => 'ProjectsToUsers',
            'className' => 'Users',
            'targetForeignKey' => 'user_id',
            'strategy' => 'subquery'
        ]);
        $this->hasMany('ProjectsToUsers');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getProjectsByUser(IdentityInterface $identity, ?int $project_id = null): Query
    {
        $query = $this->find('all')->contain(['ProjectsToUsers', 'ProjectsToUsers.Users'])->where([
            'OR' => [
                'Projects.user_id' => $identity->getIdentifier(),
                'SharedToUsers.id' => $identity->getIdentifier()
            ]
        ])->leftJoinWith('SharedToUsers')->distinct(['Projects.id']);
        if ($project_id) {
            $query->where(['Projects.id' => $project_id]);
        }
        return $query;
    }

    public function findPublic(Query $query, array $options)
    {
        return $query->where(['Projects.is_public' => true])->select(['id', 'name']);
    }

    public function beforeSave(EventInterface $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity instanceof Project && is_array($entity->projects_to_users)) {
            foreach ($entity->projects_to_users as $projects_to_user) {
                if ($projects_to_user->is_read_only && $projects_to_user->is_delete_enabled) {
                    // read only has precedence before delete permission
                    $projects_to_user->is_delete_enabled = false;
                }
            }
        }
    }
}
