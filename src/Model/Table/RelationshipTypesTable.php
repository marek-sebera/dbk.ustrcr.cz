<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\RelationshipType;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RelationshipTypes Model
 *
 * @property TypesRelationshipsTable&HasMany $TypesRelationships
 *
 * @method RelationshipType newEmptyEntity()
 * @method RelationshipType newEntity(array $data, array $options = [])
 * @method RelationshipType[] newEntities(array $data, array $options = [])
 * @method RelationshipType get($primaryKey, $options = [])
 * @method RelationshipType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method RelationshipType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RelationshipType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method RelationshipType|false save(EntityInterface $entity, $options = [])
 * @method RelationshipType saveOrFail(EntityInterface $entity, $options = [])
 * @method RelationshipType[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method RelationshipType[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method RelationshipType[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method RelationshipType[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class RelationshipTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('relationship_types');
        $this->setDisplayField('label');
        $this->setPrimaryKey('id');

        $this->hasMany('TypesRelationships', [
            'foreignKey' => 'relationship_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
