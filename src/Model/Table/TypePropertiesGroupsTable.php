<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypePropertiesGroups Model
 *
 * @property \App\Model\Table\TypesTable&\Cake\ORM\Association\BelongsTo $Types
 * @property \App\Model\Table\MetadataTypesTable&\Cake\ORM\Association\HasMany $MetadataTypes
 * @property \App\Model\Table\TypesRelationshipsTable&\Cake\ORM\Association\HasMany $TypesRelationships
 *
 * @method \App\Model\Entity\TypePropertiesGroup newEmptyEntity()
 * @method \App\Model\Entity\TypePropertiesGroup newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TypePropertiesGroup[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TypePropertiesGroupsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('type_properties_groups');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Types', [
            'foreignKey' => 'type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('MetadataTypes', [
            'foreignKey' => 'type_properties_group_id',
        ]);
        $this->hasMany('TypesRelationships', [
            'foreignKey' => 'type_properties_group_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('group_name')
            ->maxLength('group_name', 255)
            ->requirePresence('group_name', 'create')
            ->notEmptyString('group_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['type_id'], 'Types'));

        return $rules;
    }
}
