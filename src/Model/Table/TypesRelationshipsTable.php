<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\TypesRelationship;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypesRelationships Model
 *
 * @property TypesTable&BelongsTo $ToTypes
 * @property TypesTable&BelongsTo $FromTypes
 * @property RelationshipTypesTable&BelongsTo $RelationshipTypes
 * @property TypePropertiesGroupsTable&BelongsTo $TypePropertiesGroups
 *
 * @method TypesRelationship newEmptyEntity()
 * @method TypesRelationship newEntity(array $data, array $options = [])
 * @method TypesRelationship[] newEntities(array $data, array $options = [])
 * @method TypesRelationship get($primaryKey, $options = [])
 * @method TypesRelationship findOrCreate($search, ?callable $callback = null, $options = [])
 * @method TypesRelationship patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method TypesRelationship[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method TypesRelationship|false save(EntityInterface $entity, $options = [])
 * @method TypesRelationship saveOrFail(EntityInterface $entity, $options = [])
 * @method TypesRelationship[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method TypesRelationship[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method TypesRelationship[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method TypesRelationship[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TypesRelationshipsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('types_relationships');
        $this->setDisplayField('relationship_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('FromTypes', [
            'foreignKey' => 'from_type_id',
            'joinType' => 'INNER',
            'className' => 'Types'
        ]);
        $this->belongsTo('ToTypes', [
            'foreignKey' => 'to_type_id',
            'joinType' => 'INNER',
            'className' => 'Types'
        ]);
        $this->belongsTo('RelationshipTypes', [
            'foreignKey' => 'relationship_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('TypePropertiesGroups');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('relationship_name')
            ->maxLength('relationship_name', 255)
            ->requirePresence('relationship_name', 'create')
            ->notEmptyString('relationship_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['from_type_id'], 'FromTypes'));
        $rules->add($rules->existsIn(['to_type_id'], 'ToTypes'));
        $rules->add($rules->existsIn(['relationship_type_id'], 'RelationshipTypes'));

        return $rules;
    }

    public function getRelationshipByProject(?\Authentication\IdentityInterface $getIdentity, int $from_type_id, ?int $type_relationship_id): Query
    {
        $query = $this->find('all')->where(['TypesRelationships.from_type_id' => $from_type_id])->contain(['TypePropertiesGroups']);
        if ($type_relationship_id > 0) {
            $query->where(['TypesRelationships.id' => $type_relationship_id]);
        }
        return $query;
    }
}
