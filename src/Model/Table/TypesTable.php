<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Type;
use Authentication\IdentityInterface;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Types Model
 *
 * @property ProjectsTable&BelongsTo $Projects
 * @property InstancesTable&HasMany $Instances
 * @property MetadataTable&BelongsToMany $Metadata
 * @property MetadataTypesTable&HasMany $MetadataTypes
 * @property TypesRelationshipsTable&HasMany $TypesRelationships
 * @property TypePropertiesGroupsTable&HasMany $TypePropertiesGroups
 * @property TypesRelationshipsTable&BelongsToMany $ShownRelationships
 * @property MetadataTypesTable&BelongsToMany $ShownMetadata
 *
 * @method Type newEmptyEntity()
 * @method Type newEntity(array $data, array $options = [])
 * @method Type[] newEntities(array $data, array $options = [])
 * @method Type get($primaryKey, $options = [])
 * @method Type findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Type patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Type[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Type|false save(EntityInterface $entity, $options = [])
 * @method Type saveOrFail(EntityInterface $entity, $options = [])
 * @method Type[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Type[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Type[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Type[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TypesTable extends Table
{
    public const ALL_CONTAIN = [
        'Projects',
        'TypePropertiesGroups',
        'TypePropertiesGroups.TypesRelationships',
        'TypePropertiesGroups.MetadataTypes',
        'ShownMetadata'
    ];
    public const ALL_TYPES = "ALL";
    public const HELPER_TYPES = "HELPERS";
    public const STANDARD_TYPES = "STANDARD";
    public const VALID_TYPES = [
      self::ALL_TYPES,
      self::HELPER_TYPES,
      self::STANDARD_TYPES
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('types');
        $this->setDisplayField('type_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Instances', [
            'foreignKey' => 'type_id',
        ]);
        $this->belongsToMany('Metadata', [
            'foreignKey' => 'type_id',
            'targetForeignKey' => 'metadata_type_id',
            'joinTable' => 'metadata_types',
        ]);
        $this->hasMany('TypesRelationships', [
            'foreignKey' => 'from_type_id'
        ]);
        $this->hasMany('MetadataTypes');
        $this->hasMany('TypePropertiesGroups');

        $this->belongsToMany('ShownMetadata', [
            'className' => 'MetadataTypes',
            'joinTable' => 'types_shown_metadata',
            'foreignKey' => 'type_id',
            'targetForeignKey' => 'metadata_type_id'
        ]);
        $this->belongsToMany('ShownRelationships', [
            'className' => 'TypesRelationships',
            'joinTable' => 'types_shown_relationships',
            'foreignKey' => 'type_id',
            'targetForeignKey' => 'types_relationship_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        $validator
            ->boolean('is_helper')
            ->boolean('is_linked');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['project_id'], 'Projects'));

        return $rules;
    }

    public function getTypeByProject(IdentityInterface $identity, int $project_id, ?int $type_id = null): Query
    {
        $query = $this
            ->find('all')
            ->contain([
                'Projects',
                'Projects.ProjectsToUsers',
                'Projects.SharedToUsers',
                'TypePropertiesGroups',
                'ShownMetadata',
                'ShownRelationships',
                'TypesRelationships',
            ])
            ->where(['Types.project_id' => $project_id])
            ->where([
                'OR' => [
                    'SharedToUsers.id' => $identity->getIdentifier(),
                    'Projects.user_id' => $identity->getIdentifier()
                ]
            ])->leftJoinWith('Projects.SharedToUsers');
        if ($type_id > 0) {
            $query->where(['Types.id' => $type_id]);
        }
        return $query;
    }
}
