<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View;

use App\Model\Entity\Instance;
use App\Model\Entity\MetadataType;
use App\Model\Entity\TypesRelationship;
use App\Model\Table\InstancesTable;
use App\Model\Table\TypesRelationshipsTable;
use App\Model\Table\TypesTable;
use App\View\Helper\FormHelper;
use App\View\Widget\CustomDateWidget;
use Authentication\View\Helper\IdentityHelper;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\View\View;

/**
 * Application View
 *
 * Your application's default view class
 *
 * @link https://book.cakephp.org/4/en/views.html#the-app-view
 * @property-read IdentityHelper $Identity
 * @property FormHelper $Form
 */
class AppView extends View
{
    use LocatorAwareTrait;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize(): void
    {
        $this->loadHelper('Form', [
            'templates' => 'bootstrap_form',
            'className' => 'App\View\Helper\FormHelper'
        ]);

        $customDateWidget = new CustomDateWidget($this->Form->templater());
        $this->Form->addWidget('datetime', $customDateWidget);
        $this->Form->addWidget('date', $customDateWidget);

        $this->loadHelper('Authentication.Identity');
    }

    public function isProjectsDesigner(): bool
    {
        if (!$this->isUser()) {
            return false;
        }
        return $this->Identity->get('is_projects_designer') === true || $this->isSuperAdmin();
    }

    public function isUser(): bool
    {
        return $this->Identity->isLoggedIn();
    }

    public function isSuperAdmin(): bool
    {
        if (!$this->isUser()) {
            return false;
        }
        return $this->Identity->get('is_superadmin') === true;
    }

    public function getUserEmail()
    {
        return $this->isUser() ? $this->getUser('email') : null;
    }

    public function getUser(?string $path)
    {
        return $this->Identity->get($path);
    }

    public function asList(iterable $items, string $keyField = 'id', string $valueField = 'name')
    {
        $list = [];
        foreach ($items as $item) {
            $list[$item->{$keyField}] = $item->{$valueField};
        }
        return $list;
    }

    /**
     * @param Instance|EntityInterface $instance
     * @param int $project_id
     * @param MetadataType[] $metadata_types
     * @param TypesRelationship[] $types_relationships
     * @param Instance[] $types_list
     * @param array|ResultSet|Query $types
     * @param string $controlPrefix
     * @return array [form group name => group form controls as html string]
     */
    public function prepareInstanceFormControls(
        Instance|EntityInterface $instance,
        int                      $project_id,
        array|ResultSet|Query    $metadata_types,
        array|ResultSet|Query    $types_relationships,
        array|ResultSet|Query    $types_list,
        array|ResultSet|Query    $types,
        string                   $controlPrefix = ''
    ): array
    {
        /** @var int $metadata_counter */
        $metadata_counter = count($instance->metadata ?? []);
        $instancesTable = $this->fetchTable(InstancesTable::class);
        /** @var TypesRelationshipsTable $typesRelationshipsTable */
        $typesRelationshipsTable = $this->fetchTable(TypesRelationshipsTable::class);
        $rtn = [];

        foreach ($instance->type->type_properties_groups as $properties_group) {
            $group_form_controls = "";
            foreach ($metadata_types[$instance->type_id] as $metadata) {
                if ($metadata->type_properties_group_id === $properties_group->id) {
                    $group_form_controls .= MetadataType::createFormControl($this->Form, $metadata_counter++, $metadata, $instance, $controlPrefix);
                }
            }
            foreach ($types_relationships as $type_relation) {
                if ($type_relation->type_properties_group_id === $properties_group->id) {
                    if ($type_relation->to_type->is_linked) {
                        // linked type relationship, managed through in-lined form controls instead of popup
                        $group_form_controls .= "<div class='card mb-3'>";
                        $group_form_controls .= <<<EOT
<div class='card-header linked-type-form-header'>
<h3 class='card-title flex-fill d-inline'>$type_relation->relationship_name</h3>
<i class="fas fa-plus-square text-success"></i>
<i class="fas fa-minus-square text-danger"></i>
</div>
EOT;
                        $group_form_controls .= "<div class='card-body'>";

                        $relations = $instance->getCurrentRelationshipIdsByType($type_relation);
                        // add 20 complete empty forms for user to fill in case of additive action
                        for ($i = 0; $i < 20; $i++) {
                            $relations[] = 0;
                        }
                        $inner_count = 0;
                        foreach ($relations as $relation_id) {
                            /** @var Instance $inner_instance */
                            $inner_instance = $relation_id > 0 ?
                                $instancesTable
                                    ->find('all')
                                    ->where([
                                        'Instances.id' => $relation_id,
                                        'Instances.project_id' => $project_id,
                                        'Instances.type_id' => $type_relation->to_type_id
                                    ])
                                    ->contain(InstancesTable::ALL_CONTAIN)
                                    ->firstOrFail()
                                : $instancesTable->newEmptyEntity();
                            $inner_instance->project_id = $project_id;
                            $inner_instance->type_id = $type_relation->to_type_id;
                            $inner_instance->type = $types[$type_relation->to_type_id];

                            $inner_control_prefix = $controlPrefix . 'linked.' . $type_relation->id . '.' . $inner_count . '.';
                            $inner_controls = $this->prepareInstanceFormControls(
                                $inner_instance,
                                $project_id,
                                $metadata_types,
                                $typesRelationshipsTable->find('all', [
                                    'conditions' => [
                                        'TypesRelationships.from_type_id' => $type_relation->to_type_id,
                                    ],
                                    'contain' => [
                                        'ToTypes'
                                    ]
                                ]),
                                $types_list,
                                $types,
                                $inner_control_prefix
                            );

                            $invisible = ' collapsible ' . ($inner_instance->id > 0 ? '' : ' toggleInvisible');
                            $group_form_controls .= "<div class='$invisible'>"; // invisible div
                            $group_form_controls .= '<div class="card-header' . '">';
                            $group_form_controls .= $this->Form->hidden($inner_control_prefix . 'name', [
                                'value' => sprintf('auto-link-instance-%d-to-instance-%d', $instance->id, $inner_instance->id),
                                'label' => __('Název'),
                            ]);
                            $group_form_controls .= '<h3>#' . ($inner_count + 1) . '</h3>';
                            // helper to indicate user has removed the linked instance and it should be removed
                            $group_form_controls .= $this->Form->hidden($inner_control_prefix . '_is_linked', [
                                'value' => 1,
                                'templateVars' => [
                                    'class' => 'linked-visible'
                                ]
                            ]);
                            // helper field is updated via JS, it must not be locked
                            $this->Form->unlockField($inner_control_prefix . '_is_linked');
                            $this->Form->unlockField($inner_control_prefix . 'name');
                            if ($inner_instance->id > 0) {
                                $group_form_controls .= $this->Form->hidden($inner_control_prefix . 'id', [
                                    'value' => $inner_instance->id
                                ]);
                            }
                            $group_form_controls .= '</div>';
                            foreach ($inner_controls as $header => $body) {
                                $group_form_controls .= '<div class="card mb-3">';
                                $group_form_controls .= "<div class='card-header'>$header</div>";
                                $group_form_controls .= "<div class='card-body'>$body</div>";
                                $group_form_controls .= '</div>';
                            }
                            if (($inner_count + 1) === count($relations)) {
                                $group_form_controls .= '<div class="alert alert-warning">'
                                    . __('Pro přidání dalších sekcí musíte formulář nyní uložit a k úpravám se vrátit')
                                    . '</div>';
                            }
                            $group_form_controls .= '</div>'; //invisible div

                            $inner_count++;
                        }
                        $group_form_controls .= "</div></div>";
                    } else {
                        // standard relationship 1:1 or 1:N
                        $type_relation_control = TypesRelationship::createFormControl($this->Form, $type_relation, $instance, $types_list, $controlPrefix);
                        $type_add_button = $this->Html->link('<i class="fas fa-plus-square"></i>', ['action' => 'addModify', 'project_id' => $project_id, 'type_id' => $type_relation->to_type_id], ['escape' => false, 'class' => 'btn btn-success text-white mt-4', 'data-relation-type' => $type_relation->id, 'title' => __('Přidat nový objekt')]);
                        $group_form_controls .= sprintf('<div class="row no-gutters"><div class="col">%s</div><div class="ml-1">%s</div></div>', $type_relation_control, $type_add_button);
                    }
                }
            }
            $rtn[$properties_group->group_name] = $group_form_controls;
        }

        return $rtn;
    }
}
