<?php

namespace App\View\Helper;

class FormHelper extends \Cake\View\Helper\FormHelper
{

    public function control($fieldName, array $options = []): string
    {
        $class = isset($options['class']) ? $options['class'] : '';
        if ($this->isFieldError($fieldName)) {
            $class .= ' is-invalid';
        }

        if ((isset($options['type']) && $options['type'] === 'checkbox') || isset($options['checked']) || (isset($options['value']) && is_bool($options['value']))) {
            $options['templateVars']['customType'] = 'form-check';
            $options['nestedInput'] = false;
        }

        if ((isset($options['type']) && $options['type'] === 'select') || (isset($options['options']) && is_iterable($options['options']))) {
            $class .= ' select2';
        }

        $options['templateVars']['class'] = $class;
        return parent::control($fieldName, $options);
    }

}
