<?php

namespace App\View\Widget;

use Cake\View\Form\ContextInterface;
use Cake\View\Widget\BasicWidget;
use DateTime;

class CustomDateWidget extends BasicWidget
{
    public static $_attrsToRemove = ['year', 'month', 'day', 'hour', 'minute', 'meridian', 'autocomplete'];

    public function __construct($templates)
    {
        parent::__construct($templates);
    }

    public function render(array $data, ContextInterface $context): string
    {
        $data = $this->_normalizeVal($data['val'], $data);
        foreach (self::$_attrsToRemove as $attr) {
            unset($data[$attr]);
        }
        if ($data['type'] === 'datetime') {
            // datetime is not supported as such, and has been replaced by datetime-local
            $data['type'] = 'datetime-local';
        }
        return parent::render($data, $context);
    }

    public function _normalizeVal($value, $dArray)
    {
        if (empty($value))
            return $dArray;

        $format = (isset($dArray['type']) && 'date' === $dArray['type']) ? 'Y-m-d' : 'Y-m-d\TH:i:s';

        if ($value instanceof DateTime) {
            $dArray['val'] = $value->format($format);
        }

        if (is_string($value)) {
            $dArray['val'] = date($format, strtotime($value));
        }

        return $dArray;
    }

}
