<?php

use App\Model\Entity\Project;
use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var AppView $this
 * @var User[] $users
 * @var Project[] $projects
 */

?>

<div class="card m-2">
    <div class="card-header">
        <h2><?= __('Uživatelé') ?></h2>
        <div class="alert alert-info">
            <ul>
                <li>Administrátor má oprávnění upravovat oprávnění všech uživatel kromě sama sebe</li>
                <li>Možnost vytvářet projekty povoluje akce "Přidat nový projekt" a "Vytvořit ukázkový projekt"</li>
                <li>Pro nově registrované uživatele je výchozí "Není administrátor" a "Nemůže vytvářet projekty"</li>
                <li>Uživatel který není povolen se nebude moci přihlásit</li>
                <li><strong>Důležité: úprava oprávnění se projeví až po opětovném přihlášení do aplikace</strong></li>
            </ul>
        </div>
    </div>
    <div class="card-body table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>E-mail</th>
                <th>Povolen?</th>
                <th>Administrátor?</th>
                <th>Může vytvářet projekty?</th>
                <th>Akce</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($users as $user): ?>
                <?php

                ?>
                <tr>
                    <td><?= $user->id ?></td>
                    <td><?= h($user->email) ?></td>
                    <td>
                        <?= $this->element(
                            'simple_badge',
                            [
                                'bool' => $user->is_enabled,
                                'link' => [
                                    '_name' => $user->is_enabled ? 'admin_disable_user' : 'admin_enable_user',
                                    'id' => $user->id,
                                ],
                            ],
                        ) ?>
                    </td>
                    <td>
                        <?= $this->element('simple_badge', [
                            'bool' => $user->is_superadmin,
                            'link' =>
                                [
                                    '_name' => $user->is_superadmin ? 'admin_set_is_not_admin' : 'admin_set_is_admin',
                                    'id' => $user->id,
                                ],
                        ]) ?>
                    </td>
                    <td>
                        <?= $this->element(
                            'simple_badge',
                            [
                                'bool' => $user->is_projects_designer,
                                'link' => [
                                    '_name' => $user->is_projects_designer ? 'admin_set_is_not_designer' : 'admin_set_is_designer',
                                    'id' => $user->id,
                                ],
                            ],
                        ) ?>
                    </td>
                    <td>
                        <?php
                        if ($user->is_enabled) {
                            echo $this->Html->link(
                                __('Zakázat uživatele'),
                                ['_name' => 'admin_disable_user', 'id' => $user->id],
                                ['class' => 'text-danger'],
                            );
                        } else {
                            echo $this->Html->link(
                                __('Povolit uživatele'),
                                ['_name' => 'admin_enable_user', 'id' => $user->id],
                                ['class' => 'text-success'],
                            );
                        }
                        echo ', ';
                        if ($user->is_superadmin) {
                            echo $this->Html->link(
                                __('Odebrat Administrátorská Oprávnění'),
                                ['_name' => 'admin_set_is_not_admin', 'id' => $user->id],
                                ['class' => 'text-danger'],
                            );
                        } else {
                            echo $this->Html->link(
                                __('Přidělit Administrátorská Oprávnění'),
                                ['_name' => 'admin_set_is_admin', 'id' => $user->id],
                                ['class' => 'text-success'],
                            );
                        }
                        echo ', ';
                        if ($user->is_projects_designer) {
                            echo $this->Html->link(
                                __('Zakázat vytváření projektů'),
                                ['_name' => 'admin_set_is_not_designer', 'id' => $user->id],
                                ['class' => 'text-danger'],
                            );
                        } else {
                            echo $this->Html->link(
                                __('Povolit vytváření projektů'),
                                ['_name' => 'admin_set_is_designer', 'id' => $user->id],
                                ['class' => 'text-success'],
                            );
                        }
                        ?>
                    </td>
                </tr>
            <?php
            endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="card m-2">
    <div class="card-header">
        <h2><?= __('Projekty') ?></h2>
    </div>
    <div class="card-body table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Název</th>
                <th>Veřejný v API?</th>
                <th>Vlastník</th>
                <th>Uživatelé s přístupem</th>
                <th>Akce</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($projects as $project): ?>
                <tr>
                    <td><?= $project->id ?></td>
                    <td><?= h($project->name) ?></td>
                    <td>
                        <?= $this->element('simple_badge', ['bool' => $project->is_public]) ?>
                    </td>
                    <td>
                        <?= h($project->user->email) ?>
                    </td>
                    <td>
                        <?php
                        foreach ($project->projects_to_users as $project_to_user) {
                            echo h($project_to_user->user->email) . ' ';
                            if ($project_to_user->is_read_only) {
                                echo $this->element(
                                    'simple_badge',
                                    ['class' => 'badge-warning', 'contents' => __('Čtení')],
                                );
                            } else {
                                echo $this->element(
                                    'simple_badge',
                                    ['class' => 'badge-danger', 'contents' => __('Zápis')],
                                );
                            }
                            echo '<br/>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($project->is_public) {
                            echo $this->Html->link(
                                __('Skrýt v API'),
                                ['_name' => 'admin_project_set_private', 'id' => $project->id],
                                ['class' => 'text-danger'],
                            );
                        } else {
                            echo $this->Html->link(
                                __('Zveřejnit v API'),
                                ['_name' => 'admin_project_set_public', 'id' => $project->id],
                                ['class' => 'text-success'],
                            );
                        }
                        echo ', ';
                        echo $this->Html->link(
                            __('Upravit'),
                            ['_name' => 'project_edit', 'id' => $project->id],
                            ['class' => 'text-primary'],
                        );
                        ?>
                    </td>
                </tr>
            <?php
            endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
