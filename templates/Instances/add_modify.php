<?php
/**
 * @var AppView $this
 * @var Project $project
 * @var Instance $instance
 * @var Type[] $types
 * @var Type[] $typesIndexed
 * @var Instance[] $typesList
 * @var Type[] $linkedTypes
 * @var MetadataType[] $metadata_types
 * @var TypesRelationship[] $types_relationships
 */

use App\Model\Entity\Instance;
use App\Model\Entity\MetadataType;
use App\Model\Entity\Project;
use App\Model\Entity\Type;
use App\Model\Entity\TypesRelationship;
use App\View\AppView;

$this->assign('title', $instance->name ?? __('Nový objekt typu ') . $instance->type->type_name);
?>
<h1><?= $this->fetch('title') ?></h1>
<?php

echo $this->Form->create($instance);
$submitButton = $this->Form->submit(__('Uložit vše'), ['class' => 'btn btn-success mb-3']);
echo $submitButton;

echo $this->Form->control('name', ['label' => __('Název')]);
$preparedControls = $this->prepareInstanceFormControls(
    $instance,
    $project->id,
    $metadata_types,
    $types_relationships,
    $typesList,
    $typesIndexed
);
?>
<hr/>

<?php foreach ($preparedControls as $properties_group_name => $group_form_controls):
    if (empty($group_form_controls)) {
        continue;
    }
    ?>
    <div class="card mb-3">
        <h3 class="card-header"><?= $properties_group_name ?></h3>
        <div class="card-body">
            <?= $group_form_controls ?>
        </div>
    </div>
    <?= $submitButton ?>
<?php endforeach; ?>

<?php
echo $this->Form->end();
?>

<div class="d-none">
    <div class="d-flex flex-grow-1 flex-column" id="dialog">
        <iframe id="iframe" src="" class="w-100 d-flex flex-grow-1">
        </iframe>
    </div>
</div>

<style>
    .stop-scrolling {
        height: 100%;
        overflow: hidden;
    }

    .ui-widget-overlay {
        opacity: 0.7;
    }
</style>

<script type="text/javascript">
    let lastRelationId = null;

    function inIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }

    function setResult(id, name) {
        $("#dialog").dialog('close');
        console.log('lastId ' + lastRelationId);
        let $targetSelect = $("#relation-" + lastRelationId);
        console.log($targetSelect);
        $targetSelect.append(
            new Option(name, id, false, true)
        );
        $targetSelect.trigger('change');
        let isMultiple = $targetSelect.prop('multiple');
        let currentVal = $targetSelect.val();
        console.log('currentVal: ' + currentVal);
        if (isMultiple) {
            console.log('is multiple');
        } else {
            $targetSelect.val(id);
        }
    }

    function setSingleFormControlRequired($target, desired_required_state = false) {
        if (desired_required_state) {
            $target.attr('required', desired_required_state);
            $target.attr('aria-required', desired_required_state);
        }else{
            $target.removeAttr('required');
            $target.removeAttr('aria-required');
        }
    }

    function setFormControlsRequired(target, desired_required_state = false) {
        let $target = $(target);
        console.log('setFormControlsRequired')
        console.log($target);
        // currently required will get .should-be-required class if not already
        let $current_required = $("[required]", $target);
        $current_required.toggleClass('should-be-required', true);
        setSingleFormControlRequired($current_required, desired_required_state);

        // update all .should-be-required to desired status
        let $should_be_required = $(".should-be-required", $target);
        setSingleFormControlRequired($should_be_required, desired_required_state);

        // update hidden field
        let $visible_hidden_control = $(".linked-visible", $target);
        $visible_hidden_control.val(desired_required_state ? 1 : 0);
    }

    $(function () {
        let $dialogDiv = $("#dialog");
        $dialogDiv.hide();
        let $clickTargets = $("a[data-relation-type]");
        if (inIframe()) {
            $("body > nav").hide();
        }
        $clickTargets.click(function (e) {
            e.preventDefault();
            const targetURL = $(this).prop('href');
            lastRelationId = $(this).data('relation-type');
            console.log('set relation id ' + lastRelationId);
            $("#dialog").dialog({
                modal: true,
                open: function (event, ui) {
                    $('body').addClass('stop-scrolling');
                    $dialogDiv.dialog('option', 'minHeight', window.innerHeight * .99);
                    $dialogDiv.dialog('option', 'minWidth', window.innerWidth * .99);
                    $("#iframe").prop('src', targetURL);
                },
                close: function (event, ui) {
                    $('body').removeClass('stop-scrolling');
                }
            });
        });

        let $linkedFormPlus = $(".linked-type-form-header .fa-plus-square");
        let $linkedFormMinus = $(".linked-type-form-header .fa-minus-square");

        setTimeout(function () {
            let $target = $(".toggleInvisible");
            $target.toggleClass('collapse');
            setFormControlsRequired($target, false);
        })

        $linkedFormPlus.click(function (e) {
            let $target = $(".collapse:first", $(e.target).closest('.card').find('.card-body'));
            // display the element
            $target.toggleClass('collapse');
            // set all fields to required
            setFormControlsRequired($target, true);
        });

        $linkedFormMinus.click(function (e) {
            let $target = $(".collapsible", $(e.target).closest('.card').find('.card-body'))
                .not('.collapse')
                .last();
            // collapse the element
            $target.toggleClass('collapse');
            // set all fields to not-required
            setFormControlsRequired($target, false);
        });
    });
</script>
