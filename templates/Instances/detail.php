<?php

use App\Model\Entity\Instance;
use App\Model\Entity\InstanceRelationship;
use App\Model\Entity\MetadataType;
use App\Model\Entity\Project;
use App\Model\Entity\TypePropertiesGroup;
use App\Model\Entity\TypesRelationship;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $instance Instance
 * @var $project Project
 * @var $types_relationships TypesRelationship[]
 * @var $properties_groups TypePropertiesGroup[]
 * @var $reverse_relationships InstanceRelationship[]
 * @var $metadata_types MetadataType[]
 * @var $types_relationships TypesRelationship[]
 */

$htmlHelper = $this->Html;
$this->assign('title', $instance->name) ?>
<div class="row">
    <div class="col">

        <h1><?= $this->fetch('title') ?></h1>
    </div>
    <div class="col text-right">
        <?= $this->Html->link(__('Upravit'), ['_name' => 'instance_edit', 'project_id' => $project->id, 'type_id' => $instance->type_id, 'instance_id' => $instance->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php
foreach ($instance->type->type_properties_groups as $properties_group):
    ?>
    <div class="card mb-2">
        <h2 class="card-header"><?= $properties_group->group_name ?></h2>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th><?= __('Vlastnost nebo Vazba') ?></th>
                    <th><?= __('Hodnota') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($metadata_types as $metadata_type):
                    if ($metadata_type->type_properties_group_id !== $properties_group->id) {
                        continue;
                    }
                    ?>
                    <tr>
                        <td><?= $metadata_type->type_name ?></td>
                        <td><?php list($index, $metadata_by_type) = $instance->getMetadataByType($metadata_type);
                            echo $metadata_by_type ? $metadata_by_type->getValueByType(true) : '' ?></td>
                    </tr>
                <?php endforeach; ?>
                <?php
                foreach ($types_relationships as $types_relationship):
                    if ($types_relationship->type_properties_group_id !== $properties_group->id) {
                        continue;
                    }
                    ?>
                    <tr>
                        <td><?= $types_relationship->relationship_name ?></td>
                        <td><?= join(", ", array_map(function ($instance_relationship) use ($htmlHelper, $project, $instance) {
                                return $this->Html->link($instance_relationship->instance_to->name, ['_name' => 'instance_detail', 'project_id' => $project->id, 'type_id' => $instance_relationship->instance_to->type_id, 'instance_id' => $instance_relationship->instance_to_id]);
                            }, $instance->getCurrentRelationshipsByType($types_relationship))) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php
endforeach;
?>

<?php if (!empty($reverse_relationships)): ?>
    <div class="card mb-2">
        <h2 class="card-header"><?= __('Reversní vazby') ?></h2>
        <div class="card-body">
            <table class="table datatable">
                <thead>
                <tr>
                    <th><?= __('Název Vazby') ?></th>
                    <th><?= __('Vázané objekty') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $relations = [];
                foreach ($reverse_relationships as $reverse_relationship) {
                    if (!isset($relations[$reverse_relationship->types_relationship->relationship_name])) {
                        $relations[$reverse_relationship->types_relationship->relationship_name] = [];
                    }
                    $relations[$reverse_relationship->types_relationship->relationship_name][] = $this->Html->link($reverse_relationship->instance_from->name, '#');
                }
                foreach ($relations as $category => $links):
                    ?>
                    <tr>
                        <td><?= $category ?></td>
                        <td>
                            <?php foreach ($links as $link) {
                                echo $link . ', ';
                            } ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript">
    function inIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }

    $(function () {
        if (inIframe() && typeof parent.setResult === 'function') {
            parent.setResult(<?= $instance->id ?>, '<?= $instance->name ?>');
        }else{
            console.log('not iframe', typeof parent.setResult)
        }
    });
</script>
