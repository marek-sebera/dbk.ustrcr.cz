<?php
/**
 * @var AppView $this
 * @var Project $project
 * @var MetadataType $type
 * @var array $dataTypes
 */

use App\Model\Entity\DataType;
use App\Model\Entity\MetadataType;
use App\Model\Entity\Project;
use App\View\AppView;

$this->assign('title', $type->type_name ?? __('Nová vlastnost'));

echo $this->Form->create($type);
?>
<div class="card m-2">
    <div class="card-header">
        <?php
        echo $this->Form->control('type_name', ['label' => __('Název vlastnosti')]);
        ?>
    </div>
    <div class="card-body">
        <div class="card-text">
            <?php
            echo $this->Form->control('data_type_id', ['label' => __('Typ vlastnosti'), 'options' => $dataTypes]);
            ?>
            <hr/>
            <?php
            echo $this->Form->control('default_value', ['label' => __('Výchozí hodnota')]);
            ?>
            <hr/>
            <?php
            echo $this->Form->control('type_properties_group_id', ['label' => __('Skupina vlastností'), 'options' => $this->asList($type->type->type_properties_groups, 'id', 'group_name')]);
            ?>
        </div>
    </div>
    <div class="card-footer text-right">
        <?php
        echo $this->Form->button(__('Uložit'), ['class' => 'btn btn-success m-2', 'type' => 'submit']);
        echo $this->Form->end();
        if (!$type->isNew()) {
            echo $this->Form->postLink(__('Smazat vlastnost'), ['action' => 'delete', 'project_id' => $project->id, 'type_id' => $type->type_id, 'metadata_type_id' => $type->id], ['class' => 'btn btn-danger m-2', 'confirm' => __('Opravdu chcete smazat vyplněnou vlastnost?')]);
        }
        ?>
    </div>
</div>

<script type="text/javascript">
    const INPUT_SETTINGS = <?= json_encode(DataType::TYPES_TO_DEFAULT_VALUE_FORM_CONTROL_TYPE) ?>;
    let LAST_TOUCHED = {};
    $(document).ready(function () {
        $("#data-type-id").on('change', function () {
            let $selectedKey = parseInt($(this).val());
            const $defValTarget = $("input#default-value");
            if ($selectedKey in INPUT_SETTINGS) {
                let $settings = INPUT_SETTINGS[$selectedKey];
                // reset current value to be sure
                $defValTarget.val("");
                // remove all previously set
                $.each(LAST_TOUCHED, function (index, value) {
                    $defValTarget.removeProp(index);
                });
                if (typeof $settings === 'string') {
                    LAST_TOUCHED = {type: $settings};
                    $defValTarget.prop('type', $settings);
                } else {
                    LAST_TOUCHED = $settings;
                    $.each($settings, function (index, value) {
                        console.log(index, value);
                        $defValTarget.prop(index, value);
                    });
                }
            }
        }).change();
    });
</script>
