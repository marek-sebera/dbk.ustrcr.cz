<?php
/**
 * @var AppView $this
 * @var Project $project
 * @var array $usersList
 */

use App\Model\Entity\Project;
use App\View\AppView;

$this->assign('title', $project->name ? __('Úprava') : __('Nový projekt'));
?>
<div class="card m-2">
    <div class="card-header">
        <?php
        echo $this->Form->create($project);
        echo $this->Form->control('name', ['label' => __('Název projektu')]);
        echo $this->Form->control('is_public', ['label' => __('Je projekt zveřejněn v API (data jsou dostupná bez přihlášení)?'), 'type' => 'checkbox']);
        ?>
    </div>
    <?php if (!$project->isNew()): ?>
        <div class="card-body table-responsive">
            <h3><?= __('Sdílení') ?> <a href="#" id="new-share-add"><i class="fas fa-plus-circle text-success"></i></a>
            </h3>

            <div class="alert alert-info">
                <ul>
                    <li>Pokud zakážete úpravu dat, automaticky bude zakázáno upravovat strukturu i mazání dat</li>
                    <li>Samostatně pak lze povolit nebo zakázat úpravy struktury projektu (databázový design) a mazání již vložených dat (v takovém případě lze jen upravovat vložené nebo vkládat nová data)</li>
                    <li><strong>Změna nastavení oprávnění nebo sdílení se projeví okamžitě, bez nutnosti se znovu přihlašovat do aplikace</strong></li>
                </ul>
            </div>

            <table class="table">
                <thead>
                <tr>
                    <th>Uživatel (e-mail)</th>
                    <th>Zakázat upravovat data?</th>
                    <th>Může upravovat strukturu projektu?</th>
                    <th>Může mazat data?</th>
                </tr>
                </thead>
                <tbody>
                <tr id="new-share">
                    <?php
                    $formFieldPrefix = sprintf('projects_to_users.%d.', count($project->projects_to_users));
                    ?>
                    <td>
                        <?= $this->Form->control($formFieldPrefix . 'user_id', ['options' => $usersList, 'label' => false, 'empty' => __('Vyberte uživatele')]) ?>
                    </td>
                    <td><?= $this->Form->control($formFieldPrefix . 'is_read_only', ['type' => 'checkbox', 'default' => true, 'label' => false]) ?></td>
                    <td><?= $this->Form->control($formFieldPrefix . 'is_design_enabled', ['type' => 'checkbox', 'label' => false]) ?></td>
                    <td><?= $this->Form->control($formFieldPrefix . 'is_delete_enabled', ['type' => 'checkbox', 'checked' => false, 'label' => false]) ?></td>
                </tr>
                <?php $counter = 0;
                foreach ($project->projects_to_users as $projects_to_user): ?>
                    <tr>
                        <?php
                        $formFieldPrefix = sprintf('projects_to_users.%d.', $counter);
                        $counter++;
                        echo $this->Form->hidden($formFieldPrefix . 'id', ['value' => $projects_to_user->id]);
                        echo $this->Form->hidden($formFieldPrefix . 'user_id', ['value' => $projects_to_user->user_id, 'templateVars' => ['class' => 'userid']]);
                        $this->Form->unlockField($formFieldPrefix . 'user_id');
                        ?>
                        <td>
                            <a href="#" class="remove-share-row"><i class="fas fa-times-circle text-danger"></i></a>
                            <?= h($projects_to_user->user->email) ?>
                        </td>
                        <td><?= $this->Form->control($formFieldPrefix . 'is_read_only', ['type' => 'checkbox', 'checked' => $projects_to_user->is_read_only, 'label' => false, 'class' => 'readonly']) ?></td>
                        <td><?= $this->Form->control($formFieldPrefix . 'is_design_enabled', ['type' => 'checkbox', 'label' => false]) ?></td>
                        <td><?= $this->Form->control($formFieldPrefix . 'is_delete_enabled', ['type' => 'checkbox', 'checked' => $projects_to_user->is_delete_enabled, 'label' => false, 'class' => 'delete']) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
    <div class="card-footer">
        <?php
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $("#new-share").hide();
        $("#new-share-add").click(function () {
            $("#new-share").show();
        });
        $(".remove-share-row").click(function () {
            let $row = $(this).closest('tr');
            let $userid = $('.userid', $row);
            $userid.val(0);
            $row.hide();
        });
        $(".readonly").click(function () {
            if ($(this).prop('checked')) {
                let $row = $(this).closest('tr');
                let $deleteInput = $('.delete', $row);
                $deleteInput.prop('checked', !$(this).prop('checked'));
            }
        })
        $(".delete").click(function () {
            if ($(this).prop('checked')) {
                let $row = $(this).closest('tr');
                let $readOnlyInput = $('.readonly', $row);
                $readOnlyInput.prop('checked', !$(this).prop('checked'));
            }
        })
    });
</script>
