<?php
/**
 * @var AppView $this
 * @var Project $project
 */

use App\Model\Entity\DataType;
use App\Model\Entity\Project;
use App\Model\Table\TypesTable;
use App\View\AppView;

$htmlHelper = $this->Html;
$this->assign('title', $project->name);
?>
<h1><?= $this->fetch('title') ?></h1>
<hr/>

<div class="row">
    <div class="col">
        <h2><?= __('Datové typy') ?>:</h2>
    </div>
    <div class="text-right">
        <?= $this->Html->link(
            __('Přidat Datový Typ'),
            ['action' => 'addModify', 'controller' => 'Types', 'project_id' => $project->id],
            ['class' => 'btn btn-success m-2'],
        ) ?>
        <?= $this->Html->link(
            __('Přehled všech vlastností a vazeb'),
            ['action' => 'projectStructure', 'controller' => 'Projects', 'id' => $project->id],
            ['class' => 'btn btn-primary m-2'],
        ) ?>
    </div>
</div>
<hr/>
<nav>
    <ul class="nav nav-tabs" id="types-tab" role="tablist">
        <?php
        foreach ($project->orderedTypes(TypesTable::STANDARD_TYPES) as $type): ?>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="nav-tab-<?= $type->id ?>" data-toggle="tab" href="#tab-<?= $type->id ?>"
                   role="tab"
                   aria-controls="tab-<?= $type->id ?>" aria-selected="true"><?= $type->type_name ?></a>
            </li>
        <?php
        endforeach; ?>
        <?php
        $orderedHelperTypes = $project->orderedTypes(TypesTable::HELPER_TYPES);
        if (!empty($orderedHelperTypes)):
            ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                   aria-expanded="false"><?= __('Pomocné datové typy') ?></a>
                <div class="dropdown-menu">
                    <?php
                    foreach ($orderedHelperTypes as $type): ?>
                        <a class="dropdown-item" id="nav-tab-<?= $type->id ?>" data-toggle="tab"
                           href="#tab-<?= $type->id ?>"
                           role="tab"
                           aria-controls="tab-<?= $type->id ?>" aria-selected="true"><?= $type->type_name ?></a>
                    <?php
                    endforeach; ?>
                </div>
            </li>
        <?php
        endif; ?>
    </ul>
</nav>
<div class="tab-content" id="myTabContent">
    <?php
    foreach ($project->orderedTypes() as $type): ?>
        <div class="tab-pane fade" id="tab-<?= $type->id ?>" role="tabpanel" aria-labelledby="tab-<?= $type->id ?>">
            <div class="card mb-2">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h3>
                                <?= sprintf("%s", $type->type_name) ?>
                            </h3>
                        </div>
                        <div class="col text-right">
                            <?= $this->Html->link(
                                __('Zobrazit tabulku všech informací'),
                                ['_name' => 'types_everything', 'type_id' => $type->id, 'project_id' => $project->id],
                                ['class' => 'btn btn-primary ml-2 mr-2'],
                            ) ?>
                            <?= $this->Html->link(
                                __('Upravit typ'),
                                ['_name' => 'types_edit', 'project_id' => $project->id, 'type_id' => $type->id],
                                ['class' => 'btn btn-warning ml-2 mr-2'],
                            ) ?>
                            <?= $type->is_linked ? '' : $this->Html->link(
                                sprintf("%s '%s'", __('Přidat'), $type->type_name),
                                ['_name' => 'instance_add', 'project_id' => $project->id, 'type_id' => $type->id],
                                ['class' => 'btn btn-success'],
                            ) ?>
                        </div>
                    </div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table datatable">
                        <thead>
                        <tr>
                            <?php
                            if (empty($type->shown_metadata) && empty($type->shown_relationships)): ?>
                                <th><?= __('Název') ?></th>
                            <?php
                            else: ?>
                                <?php
                                foreach ($type->shown_metadata as $shown_metadata): ?>
                                    <th data-type="<?= DataType::dataTableColumnType(
                                        $shown_metadata->data_type_id,
                                    ) ?>"><?= $shown_metadata->type_name ?></th>
                                <?php
                                endforeach; ?>
                                <?php
                                foreach ($type->shown_relationships as $shown_relationship): ?>
                                    <th data-type="html"><?= $shown_relationship->relationship_name ?></th>
                                <?php
                                endforeach; ?>
                            <?php
                            endif; ?>
                            <th><?= __('Akce') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($type->instances as $instance): ?>
                            <tr>
                                <?php
                                if (empty($type->shown_metadata) && empty($type->shown_relationships)): ?>
                                    <td><?= $this->Html->link(
                                            $instance->name,
                                            [
                                                '_name' => 'instance_detail',
                                                'instance_id' => $instance->id,
                                                'project_id' => $project->id,
                                                'type_id' => $type->id,
                                            ],
                                            ['class' => 'text-primary'],
                                        ) ?></td>
                                <?php
                                else: ?>
                                    <?php
                                    foreach ($type->shown_metadata as $shown_metadata): ?>
                                        <td><?php
                                            [$index, $found] = $instance->getMetadataByType($shown_metadata);
                                            echo $found ? $found->getValueByType(true) : '' ?></td>
                                    <?php
                                    endforeach; ?>
                                    <?php
                                    foreach ($type->shown_relationships as $shown_relationship): ?>
                                        <td><?= join(
                                                ", ",
                                                array_map(
                                                    function ($instance_relationship) use (
                                                        $htmlHelper,
                                                        $project,
                                                        $instance,
                                                    ) {
                                                        return $this->Html->link(
                                                            $instance_relationship->instance_to->name,
                                                            [
                                                                '_name' => 'instance_detail',
                                                                'project_id' => $project->id,
                                                                'type_id' => $instance_relationship->instance_to->type_id,
                                                                'instance_id' => $instance_relationship->instance_to_id,
                                                            ],
                                                        );
                                                    },
                                                    $instance->getCurrentRelationshipsByType($shown_relationship),
                                                ),
                                            ) ?></td>
                                    <?php
                                    endforeach; ?>
                                <?php
                                endif; ?>
                                <td>
                                    <?= $this->Html->link(
                                        __('Otevřít'),
                                        [
                                            '_name' => 'instance_detail',
                                            'instance_id' => $instance->id,
                                            'project_id' => $project->id,
                                            'type_id' => $type->id,
                                        ],
                                        ['class' => 'text-primary'],
                                    ) ?>
                                    ,
                                    <?= $this->Html->link(
                                        __('Upravit'),
                                        [
                                            '_name' => 'instance_edit',
                                            'instance_id' => $instance->id,
                                            'project_id' => $project->id,
                                            'type_id' => $type->id,
                                        ],
                                        ['class' => 'text-success'],
                                    ) ?>
                                    ,
                                    <?= $this->Form->postLink(
                                        __('Smazat'),
                                        [
                                            '_name' => 'instance_delete',
                                            'instance_id' => $instance->id,
                                            'project_id' => $project->id,
                                            'type_id' => $type->id,
                                        ],
                                        [
                                            'class' => 'text-danger',
                                            'confirm' => __('Opravdu chcete smazat tento objekt?'),
                                        ],
                                    ) ?>
                                </td>
                            </tr>
                        <?php
                        endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <?= __('Skupiny vlastností') ?>:
                    <?= $this->Html->link(
                        __('Přidat novou skupinu'),
                        ['_name' => 'type_properties_groups_add', 'project_id' => $project->id, 'type_id' => $type->id],
                        ['class' => 'text-success'],
                    ); ?>
                    <hr/>
                    <?php
                    foreach ($type->type_properties_groups as $type_properties_group) { ?>
                        Skupina: <?= $this->Html->link(
                            $type_properties_group->group_name,
                            [
                                '_name' => 'type_properties_groups_edit',
                                'project_id' => $project->id,
                                'type_id' => $type->id,
                                'group_id' => $type_properties_group->id,
                            ],
                        ); ?>
                        <div>
                            <?= __('Vlastnosti') ?>:
                            <?= $this->Html->link(
                                __('Přidat vlastnost'),
                                [
                                    '_name' => 'metadata_types_add',
                                    'project_id' => $project->id,
                                    'type_id' => $type->id,
                                    'group_id' => $type_properties_group->id,
                                ],
                                ['class' => 'text-success'],
                            ) ?>
                            ,
                            <?php
                            foreach ($type->metadata_types as $metadata_type) {
                                if ($metadata_type->type_properties_group_id === $type_properties_group->id) {
                                    echo $this->Html->link(
                                        $metadata_type->type_name,
                                        [
                                            '_name' => 'metadata_types_edit',
                                            'project_id' => $project->id,
                                            'type_id' => $type->id,
                                            'metadata_type_id' => $metadata_type->id,
                                        ],
                                    );
                                    echo ', ';
                                }
                            }
                            ?>
                            <br/>
                            <?= __('Vazby') ?>:
                            <?= $this->Html->link(
                                __('Přidat vazbu'),
                                [
                                    '_name' => 'type_relationships_add',
                                    'project_id' => $project->id,
                                    'type_id' => $type->id,
                                    'group_id' => $type_properties_group->id,
                                ],
                                ['class' => 'text-success'],
                            ) ?>
                            ,
                            <?php
                            foreach ($type->types_relationships as $type_relationship) {
                                if ($type_relationship->type_properties_group_id === $type_properties_group->id) {
                                    echo $this->Html->link(
                                        sprintf(
                                            "%s (%s)",
                                            $type_relationship->relationship_name,
                                            $type_relationship->to_type->type_name
                                        ),
                                        [
                                            '_name' => 'type_relationships_edit',
                                            'project_id' => $project->id,
                                            'type_id' => $type->id,
                                            'type_relationship_id' => $type_relationship->id,
                                        ],
                                    );
                                    echo ', ';
                                }
                            }
                            ?>
                        </div>
                        <hr/>
                    <?php
                    } ?>
                </div>
            </div>
        </div>
    <?php
    endforeach; ?>
</div>

<script type="text/javascript">
    $(function () {
        let $hash = window.location.hash;
        if ($hash.startsWith('#tab-')) {
            $('#nav-' + $hash.slice(1)).tab('show');
        } else {
            $('#types-tab > li:first > a').tab('show');
        }
    });
</script>
