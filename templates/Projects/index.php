<?php
/**
 * @var AppView $this
 * @var Project[] $projects
 */

use App\Model\Entity\Project;
use App\View\AppView;

$this->assign('title', __('Projekty'));
?>
<div class="row">
    <div class="col h2">
        <?= __('Přehled projektů') ?>
    </div>
    <div class="col text-right">
        <?php if ($this->isProjectsDesigner()): ?>
            <?= $this->Html->link(__('Přidat nový projekt'), ['action' => 'addModify'], ['class' => 'btn btn-success m-2']) ?>
            <?= $this->Html->link(__('Vytvořit ukázkový projekt'), ['action' => 'addExample'], ['class' => 'btn btn-dark m-2']) ?>
        <?php endif; ?>
    </div>
</div>
<hr/>
<?php foreach ($projects as $project): ?>
    <div class="card h-100 mb-4">
        <h2 class="card-header">
            <?= $this->Html->link($project->name, ['action' => 'detail', $project->id], ['class' => 'text-dark']) ?>
        </h2>
        <div class="card-body">
            <div class="card-text">
                <?= $this->Html->link(__('Otevřit'), ['_name' => 'project_detail', $project->id], ['class' => 'btn btn-success']) ?>
                <?= $this->Html->link(__('Upravit'), ['_name' => 'project_edit', 'id' => $project->id], ['class' => 'btn btn-primary']) ?>
                <?php
                if ($this->getUser('id') === $project->user_id) {
                    echo $this->Form->postLink(__('Smazat'), ['_name' => 'project_delete', $project->id], ['class' => 'btn btn-danger', 'confirm' => __('Opravdu chcete smazat tento projekt?')]);
                }
                ?>
            </div>
        </div>
        <div class="card-footer">
            <?= sprintf("%s: %s", __('Autor'), h($project->user->email)) ?>
        </div>
    </div>
<?php endforeach; ?>
<?php if (empty($projects ?? []) && !$this->isProjectsDesigner()): ?>
    <div class="alert alert-danger">
        <?= __('Nemáte povoleno vytvářet projekty ani nemáte žádný k dispozici, kontaktujte prosím správce této aplikace, pokud to není správně') ?>
    </div>
<?php endif; ?>
