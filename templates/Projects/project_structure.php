<?php

use App\Model\Entity\Project;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $project Project
 * @var $countByType array (int => int)
 * @var $dataTypes array (int => string)
 */
$this->assign('title', __('Přehled všech vlastností a vazeb'));
?>

<table class="table">
    <thead>
    <tr>
        <th class="text-nowrap"><?= __('Datový Typ') ?></th>
        <th class="text-nowrap"><?= __('Počet objektů') ?></th>
        <th class="text-nowrap"><?= __('Vlastnosti a vazby podle skupin') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($project->types as $type) { ?>
        <tr>
            <td>
                <?= $type->type_name ?>
            </td>
            <td>
                <?= $countByType[$type->id] ?? 0 ?>
            </td>
            <td>
                <?= $this->Html->link(__('Přidat skupinu vlastností/vazeb'), ['_name' => 'type_properties_groups_add', 'project_id' => $project->id, 'type_id' => $type->id], ['class' => 'text-success']) ?>
                <br/>
                <hr/>
                <?php foreach ($type->type_properties_groups as $type_properties_group) { ?>
                    Skupina: <?= $this->Html->link($type_properties_group->group_name, ['_name' => 'type_properties_groups_edit', 'project_id' => $project->id, 'type_id' => $type->id, 'group_id' => $type_properties_group->id]); ?>
                    <div>
                        <?= __('Vlastnosti') ?>:<br/>
                        <?php
                        foreach ($type->metadata_types as $metadata_type) {
                            if ($metadata_type->type_properties_group_id === $type_properties_group->id) {
                                echo $this->Html->link(sprintf("%s - %s", $metadata_type->type_name, $dataTypes[$metadata_type->data_type_id]), $metadata_type->data_type_id, ['_name' => 'metadata_types_edit', 'project_id' => $project->id, 'type_id' => $type->id, 'metadata_type_id' => $metadata_type->id]);
                                echo '<br/>';
                            }
                        }
                        echo $this->Html->link(__('Přidat vlastnost'), ['_name' => 'metadata_types_add', 'project_id' => $project->id, 'type_id' => $type->id, 'group_id' => $type_properties_group->id], ['class' => 'text-success'])
                        ?>
                        <br/>
                        <br/>
                        <?= __('Vazby') ?>:<br/>
                        <?php
                        foreach ($type->types_relationships as $type_relationship) {
                            if ($type_relationship->type_properties_group_id === $type_properties_group->id) {
                                echo $this->Html->link(sprintf("%s - %s", $type_relationship->relationship_name, $type_relationship->to_type->type_name), ['_name' => 'type_relationships_edit', 'project_id' => $project->id, 'type_id' => $type->id, 'type_relationship_id' => $type_relationship->id]);
                                echo '<br/>';
                            }
                        }
                        echo $this->Html->link(__('Přidat vazbu'), ['_name' => 'type_relationships_add', 'project_id' => $project->id, 'type_id' => $type->id, 'group_id' => $type_properties_group->id], ['class' => 'text-success'])
                        ?>
                    </div>
                    <hr/>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
