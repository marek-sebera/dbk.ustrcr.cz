<?php
/**
 * @var AppView $this
 * @var Type $type
 * @var TypePropertiesGroup $group
 */

use App\Model\Entity\Type;
use App\Model\Entity\TypePropertiesGroup;
use App\View\AppView;

$this->assign('title', $group->group_name ?? __('Nová skupiny vlastností'));

echo $this->Form->create($group);
?>
<div class="card m-2">
    <div class="card-header">
        <?php
        echo $this->Form->control('group_name', ['label' => __('Název skupiny vlastností')]);
        ?>
    </div>
    <div class="card-footer text-right">
        <?php
        echo $this->Form->button(__('Uložit'), ['class' => 'btn btn-success m-2', 'type' => 'submit']);
        echo $this->Form->end();

        if (!$group->isNew()) {
            echo $this->Html->link(__('Smazat skupinu vlastností'), ['action' => 'delete', 'project_id' => $type->project_id, 'type_id' => $type->id, 'group_id' => $group->id], ['class' => 'btn btn-danger m-2', 'confirm' => __('Opravdu chcete smazat skupinu včetně obsažených vlastností a vazeb?')]);
        }
        ?>
    </div>
</div>
