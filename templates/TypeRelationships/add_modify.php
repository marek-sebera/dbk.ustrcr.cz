<?php
/**
 * @var AppView $this
 * @var Project $project
 * @var Type $fromType
 * @var TypesRelationship $type
 * @var array $relationshipTypes
 * @var array $types
 * @var array $instance_listing
 */

use App\Model\Entity\Project;
use App\Model\Entity\Type;
use App\Model\Entity\TypesRelationship;
use App\View\AppView;

$this->assign('title', $type->relationship_name ?? __('Nová datová vazba'));

echo $this->Form->create($type);
?>
<div class="card m-2">
    <div class="card-header">
        <div class="row">
            <div class="col-md">
                <?php
                echo $this->Form->control('relationship_name', ['label' => sprintf("\"%s\" %s", $fromType->type_name, __('má tuto vlastnost'))]);
                ?>
            </div>
            <div class="col-md">
                <?php
                echo $this->Form->control('to_type_id', ['label' => __('Co obsahuje vlastnost / vazbu'), 'options' => [$fromType->id => $fromType->type_name], 'disabled' => 'disabled']);
                ?>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-text">
            <div class="row">
                <div class="col-md">
                    <?php
                    echo $this->Form->control('to_type_id', ['label' => __('Typ vlastnosti'), 'options' => $types]);
                    ?>
                </div>
                <div class="col-md">
                    <?php
                    echo $this->Form->control('relationship_type_id', ['label' => __('Počet odpovědí'), 'options' => $relationshipTypes]);
                    ?>
                </div>
            </div>
            <hr/>
            <?php
            echo $this->Form->control('default_value', ['type' => 'select', 'options' => [], 'multiple' => false, 'label' => __('Výchozí hodnota')]);
            ?>
            <hr/>
            <?php
            echo $this->Form->control('type_properties_group_id', ['label' => __('Do jaké skupiny patří vlastnost'), 'options' => $this->asList($fromType->type_properties_groups, 'id', 'group_name')]);
            ?>
        </div>
    </div>
    <div class="card-footer text-right">
        <?php
        echo $this->Form->button(__('Uložit'), ['type' => 'submit', 'class' => 'btn btn-success m-2']);
        echo $this->Form->end();
        if (!$type->isNew()) {
            echo $this->Form->postLink(__('Smazat vazbu'), ['action' => 'delete', 'project_id' => $project->id, 'type_relationship_id' => $type->id, 'type_id' => $type->from_type_id], ['class' => 'btn btn-danger m-2', 'confirm' => __('Opravdu chcete smazat vyplněnou vazbu?')]);
        }
        ?>
    </div>
</div>

<script type="text/javascript">
    const ALL_LISTINGS = <?= json_encode($instance_listing) ?>;
    const CURRENT_DEFAULT = <?= intval($type->default_value) ?>;
    const CURRENT_DEFAULT_TYPE = <?= intval($type->to_type_id) ?>;
    $(function () {
        $("select#to-type-id").change(function () {
            let currentVal = parseInt($(this).val());
            console.log('type: ' + currentVal);
            let possibleAnswers = (currentVal in ALL_LISTINGS) ? ALL_LISTINGS[currentVal] : [];
            let $selectTarget = $("select#default-value");
            $selectTarget.val(null).trigger('change');
            $selectTarget.empty();
            $selectTarget.append(new Option('Vyberte si z možností, pokud chcete nastavit výchozí hodnotu této vazby, nebo ponechte prázdné'));
            $.each(possibleAnswers, function (index, value) {
                $selectTarget.append(new Option(value, index, false, false));
            });
            if (CURRENT_DEFAULT_TYPE === currentVal && CURRENT_DEFAULT > 0) {
                $selectTarget.val(CURRENT_DEFAULT);
            }
            $selectTarget.trigger('change');
        }).change();
    });
</script>
