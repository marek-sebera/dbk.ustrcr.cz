<?php
/**
 * @var AppView $this
 * @var Type $type
 * @var array $metadata_list
 * @var array $relationships_list
 */

use App\Model\Entity\Type;
use App\View\AppView;

$this->assign('title', $type->type_name ? __('Úprava') : __('Nový datový typ'));

echo $this->Form->create($type);
echo $this->Form->control('type_name', ['label' => __('Název datového typu')]);
echo $this->Form->control('is_helper', ['type' => 'checkbox', 'label' => __('Jde o pomocný datový typ? (Nezobrazuje se na detailu projektu)')]);
echo $this->Form->control('is_linked', ['type' => 'checkbox', 'label' => __('Jde o vázaný datový typ? (Přidávání a řazení vázaných záznamů probíhá ve formuláři místo pop-up dialogu')]);
if (!$type->isNew()) {
    echo $this->Form->control('shown_metadata._ids', ['label' => __('Vlastnosti zobrazené na jednoduchém výpisu'), 'options' => $metadata_list]);
    echo $this->Form->control('shown_relationships._ids', ['label' => __('Vazby zobrazené na jednoduchém výpisu'), 'options' => $relationships_list]);
}
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();

if (!$type->isNew()) {
    echo $this->Form->postLink(__('Smazat datový typ'), ['action' => 'delete', 'project_id' => $type->project_id, 'type_id' => $type->id], ['class' => 'btn btn-danger mt-3', 'confirm' => __('Opravdu chcete smazat datový typ i se všemi objekty?')]);
}
