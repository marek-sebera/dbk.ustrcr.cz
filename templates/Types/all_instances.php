<?php

use App\Model\Entity\Instance;
use App\Model\Entity\Type;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $instances Instance[]
 * @var $type Type
 */

$this->assign('title', __('Tabulka všech informací'));
?>
<table class="table datatable m-5 p-5">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <?php foreach ($type->type_properties_groups as $properties_group) {
            foreach ($properties_group->metadata_types as $metadata_type) {
                ?>
                <th><?= $metadata_type->type_name ?></th>
                <?php
            }
            foreach ($properties_group->types_relationships as $types_relationship) {
                ?>
                <th><?= $types_relationship->relationship_name ?></th>
                <?php
            }
        } ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($instances as $instance): ?>
        <tr>
            <td><?= $instance->id ?></td>
            <?php foreach ($type->type_properties_groups as $properties_group) {
                foreach ($properties_group->metadata_types as $metadata_type) {
                    ?>
                    <td><?= $instance->getMetadataValueOrNull($metadata_type, true) ?></td>
                    <?php
                }
                foreach ($properties_group->types_relationships as $types_relationship) {
                    ?>
                    <td><?= join(", ", array_map(function ($instance_relationship) use ($instance) {
                            return $instance_relationship->instance_to->name;
                        }, $instance->getCurrentRelationshipsByType($types_relationship))) ?></td>
                    <?php
                }
            } ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
