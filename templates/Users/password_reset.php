<?php
/**
 * @var $user User
 * @var $this AppView
 **/

use App\Model\Entity\User;
use App\View\AppView;

echo $this->Form->create($user);
echo $this->Form->control('email', ['label' => __('E-mailová adresa')]);
echo $this->Form->submit(__('Obnovit heslo'), ['class' => 'btn btn-success']);
echo $this->Form->end();
