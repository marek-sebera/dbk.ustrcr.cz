<?php
/**
 * @var $user User
 * @var $this AppView
 **/

use App\Model\Entity\User;
use App\View\AppView;

echo $this->Form->create(null);
echo $this->Form->control('password', ['label' => __('Nové heslo'), 'required' => 'required']);
echo $this->Form->submit(__('Nastavit nové heslo'), ['class' => 'btn btn-success']);
echo $this->Form->end();
