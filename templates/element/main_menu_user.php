<?php
/**
 * @var $this AppView
 */

use App\View\AppView;

?>

<ul class="navbar-nav ml-auto">
    <?php if ($this->isSuperAdmin()): ?>
        <li class="nav-item">
            <?= $this->Html->link(__('Administrace'), ['_name' => 'admin_dashboard'], ['class' => 'btn btn-outline-success mr-2']) ?>
        </li>
    <?php endif; ?>

    <li class="nav-item dropdown">
        <a href="#" id="dropdownUserSettings" class="dropdown-toggle nav-link" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i> <?= h($this->getUserEmail()) ?>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUserSettings">
            <?php $this->Html->link('<i class="fas fa-cogs"></i> ' . __('Moje informace'), ['_name' => 'update_self_info'], ['class' => 'dropdown-item', 'escape' => false]) ?>
            <!--<div class="dropdown-divider"></div>-->
            <?php $this->Html->link('<i class="fas fa-key"></i> ' . __('Změnit heslo'), ['_name' => 'change_password'], ['class' => 'dropdown-item', 'escape' => false]) ?>
            <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i> ' . __('Odhlásit se'), ['_name' => 'logout'], ['class' => 'dropdown-item', 'escape' => false]) ?>
        </div>
    </li>
</ul>
