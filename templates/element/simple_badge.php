<?php

/**
 * @var AppView $this
 * @var string $contents
 * @var string $class
 * @var string $link
 * @var bool $bool
 */

use App\View\AppView;

if (isset($bool)) {
    if ($bool === true) {
        $class = 'badge-success';
        if (!isset($contents)) {
            $contents = __('Ano');
        }
    } else {
        $class = 'badge-danger';
        if (!isset($contents)) {
            $contents = __('Ne');
        }
    }
    if (isset($link)) {
        $contents = $this->Html->link($contents, $link);
    }
}

if (empty($class)) {
    $class = 'badge-dark';
}
?>
<div class="badge <?= $class ?>">
    <?= $contents ?>
</div>
