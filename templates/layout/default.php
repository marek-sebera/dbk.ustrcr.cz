<?php
/**
 * @var $this AppView
 */

use App\View\AppView;
use Cake\Routing\Router;

$this->Html->css([
    'bootstrap.min.css',
    'select2.min.css',
    'datatables.min.css',
    'fontawesome.min.css',
    'jquery-ui.min.css',
    'jquery.datetimepicker.min.css',
    'dbk.css?rand='.rand()
], ['block' => true]);

$this->Html->script([
    'jquery-3.5.1.min.js',
    'popper.min.js',
    'bootstrap.min.js',
    'popper.min.js',
    'select2.min.js',
    'select2.auto.js',
    'pdfmake.min.js',
    'vfs_fonts.js',
    'datatables.min.js',
    'datatables.czech-string.js',
    'datatables.intl.js',
    'simple_datatable.js',
    'fontawesome.min.js',
    'jquery-ui.min.js',
    'jquery.datetimepicker.full.min.js',
    'modernizr-datetime.js',
], ['block' => true]);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>
        <?= $this->fetch('title') ?> - Databáze K
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">
        <?= $this->Html->image('logo.png', ['height' => 30, 'width' => 30]) ?>
        Databáze K
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php
        if ($this->isUser()) {
            echo $this->element('main_menu_user');
        } else {
            echo $this->element('main_menu_not_user');
        }
        ?>
    </div>
</nav>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <?= $this->Html->link(__('Hlavní stránka'), '/') ?>
        </li>

        <?php if (isset($crumbs)): ?>
            <?php foreach ($crumbs as $crumb_name => $crumb_path): ?>
                <li class="breadcrumb-item">
                    <?= $this->Html->link($crumb_name, (is_string($crumb_path) && Router::routeExists(['_name' => $crumb_path])) ? ['_name' => $crumb_path] : $crumb_path) ?>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if ($this->getRequest() && $this->getRequest()->getRequestTarget() !== '/'): ?>
            <li class="breadcrumb-item active" aria-current="page">
                <?= $this->Html->link($this->fetch('title'), $this->getRequest()->getRequestTarget()) ?>
            </li>
        <?php endif; ?>
    </ol>
</nav>

<div class="container-fluid clearfix mt-2">
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>
</div>

</body>
</html>
