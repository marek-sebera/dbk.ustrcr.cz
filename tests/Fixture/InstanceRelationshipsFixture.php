<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InstanceRelationshipsFixture
 */
class InstanceRelationshipsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'types_relationship_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'instance_from_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'instance_to_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'types_relationship_id' => ['type' => 'index', 'columns' => ['types_relationship_id'], 'length' => []],
            'instance_from_id' => ['type' => 'index', 'columns' => ['instance_from_id'], 'length' => []],
            'instance_to_id' => ['type' => 'index', 'columns' => ['instance_to_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'types_relationship_uniq' => ['type' => 'unique', 'columns' => ['types_relationship_id', 'instance_from_id', 'instance_to_id'], 'length' => []],
            'instance_relationships_ibfk_1' => ['type' => 'foreign', 'columns' => ['instance_from_id'], 'references' => ['instances', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'instance_relationships_ibfk_2' => ['type' => 'foreign', 'columns' => ['instance_to_id'], 'references' => ['instances', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'instance_relationships_ibfk_3' => ['type' => 'foreign', 'columns' => ['types_relationship_id'], 'references' => ['instances', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_general_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'types_relationship_id' => 1,
                'instance_from_id' => 1,
                'instance_to_id' => 1,
            ],
        ];
        parent::init();
    }
}
