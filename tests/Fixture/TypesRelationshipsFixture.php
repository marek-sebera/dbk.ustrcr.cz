<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TypesRelationshipsFixture
 */
class TypesRelationshipsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'relationship_name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'comment' => '', 'precision' => null],
        'from_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'to_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'relationship_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'created' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'from_type_id' => ['type' => 'index', 'columns' => ['from_type_id'], 'length' => []],
            'to_type_id' => ['type' => 'index', 'columns' => ['to_type_id'], 'length' => []],
            'relationship_type_id' => ['type' => 'index', 'columns' => ['relationship_type_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'types_relationships_ibfk_1' => ['type' => 'foreign', 'columns' => ['from_type_id'], 'references' => ['types', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'types_relationships_ibfk_2' => ['type' => 'foreign', 'columns' => ['to_type_id'], 'references' => ['types', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'types_relationships_ibfk_3' => ['type' => 'foreign', 'columns' => ['relationship_type_id'], 'references' => ['relationship_types', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_general_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'relationship_name' => 'Lorem ipsum dolor sit amet',
                'from_type_id' => 1,
                'to_type_id' => 1,
                'relationship_type_id' => 1,
                'modified' => '2020-05-27 12:58:18',
                'created' => '2020-05-27 12:58:18',
            ],
        ];
        parent::init();
    }
}
