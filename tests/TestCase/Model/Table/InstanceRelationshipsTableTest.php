<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstanceRelationshipsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstanceRelationshipsTable Test Case
 */
class InstanceRelationshipsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\InstanceRelationshipsTable
     */
    protected $InstanceRelationships;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.InstanceRelationships',
        'app.TypesRelationships',
        'app.Instances',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InstanceRelationships') ? [] : ['className' => InstanceRelationshipsTable::class];
        $this->InstanceRelationships = TableRegistry::getTableLocator()->get('InstanceRelationships', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->InstanceRelationships);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
