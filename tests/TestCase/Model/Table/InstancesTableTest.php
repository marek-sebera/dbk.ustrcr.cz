<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstancesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstancesTable Test Case
 */
class InstancesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\InstancesTable
     */
    protected $Instances;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Instances',
        'app.Projects',
        'app.Types',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Instances') ? [] : ['className' => InstancesTable::class];
        $this->Instances = TableRegistry::getTableLocator()->get('Instances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Instances);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
