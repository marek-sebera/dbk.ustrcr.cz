<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetadataTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetadataTable Test Case
 */
class MetadataTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MetadataTable
     */
    protected $Metadata;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Metadata',
        'app.Objects',
        'app.MetadataTypes',
        'app.Types',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Metadata') ? [] : ['className' => MetadataTable::class];
        $this->Metadata = TableRegistry::getTableLocator()->get('Metadata', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Metadata);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
