<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetadataTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetadataTypesTable Test Case
 */
class MetadataTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MetadataTypesTable
     */
    protected $MetadataTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.MetadataTypes',
        'app.Types',
        'app.DataTypes',
        'app.Projects',
        'app.Metadata',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MetadataTypes') ? [] : ['className' => MetadataTypesTable::class];
        $this->MetadataTypes = TableRegistry::getTableLocator()->get('MetadataTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MetadataTypes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
