<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RelationshipTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelationshipTypesTable Test Case
 */
class RelationshipTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RelationshipTypesTable
     */
    protected $RelationshipTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.RelationshipTypes',
        'app.TypesRelationships',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RelationshipTypes') ? [] : ['className' => RelationshipTypesTable::class];
        $this->RelationshipTypes = TableRegistry::getTableLocator()->get('RelationshipTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->RelationshipTypes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
