<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypePropertiesGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypePropertiesGroupsTable Test Case
 */
class TypePropertiesGroupsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TypePropertiesGroupsTable
     */
    protected $TypePropertiesGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.TypePropertiesGroups',
        'app.Types',
        'app.MetadataTypes',
        'app.TypesRelationships',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypePropertiesGroups') ? [] : ['className' => TypePropertiesGroupsTable::class];
        $this->TypePropertiesGroups = TableRegistry::getTableLocator()->get('TypePropertiesGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TypePropertiesGroups);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
