$(document).ready(function () {
    let collator;
    try {
        $.fn.dataTable.ext.order.intl('cs', {
            'numeric': true
        });
    } catch (err) {
        console.log(err);
        $.fn.dataTable.ext.order.intl();
    }

    $(".datatable").DataTable({
        stateSave: true,
        paging: false,
        autoWidth: true,
        colReorder: true,
        dom: "<'border-top pb-2'><'row'<'col'l><'col'B><'col'f>>rtip",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        pageLength: 200,
        lengthMenu: [20, 50, 100, 150, 200, 250, 300, 500, 1000]
    });
})
